﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using PetClinic.Domain.Models;
using PetClinic.Infrastructure.Repositories;
using PetClinic.Service.Services;
using PetClinic.Tests.Infrastructure;
using PetClinic.Tests.Infrastructure.DatabaseContextArrangeExtensions;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace PetClinic.Services.IntegrationTests.Services
{
    public class VisitServiceIntegrationTests : IntegrationTestBase<TestStartup>
    {
        private readonly VisitService _sut;

        public VisitServiceIntegrationTests()
        {
            var logger = Mock.Of<ILogger<VisitService>>();
            VisitRepository visitRepository = new VisitRepository(DatabaseContext, Mock.Of<ILogger<VisitRepository>>());
            ApplicationUserRepository applicationUserRepository = new ApplicationUserRepository(DatabaseContext, Mock.Of<ILogger<ApplicationUserRepository>>());

            _sut = new VisitService(visitRepository, applicationUserRepository, logger);
        }

        [Fact]
        public async void create_visit_should_be_saved()
        {
            // arrange
            var vet = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var customer = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            await DatabaseContext.SaveChangesAsync();

            var createVisitModel = VisitArrangeExtension.CreateVisitModel(
                veterinarianId: vet.Id,
                customerId: customer.Id,
                animalId: customer.ClinicUser.CustomerInfo.Animals.FirstOrDefault()?.Id
                );

            // act
            var result = await _sut.CreateVisit(createVisitModel);

            // assert
            result.Veterinarian.Should().BeEquivalentTo(vet);
            result.Customer.Should().BeEquivalentTo(customer);
            result.Animal.Should().BeEquivalentTo(customer.ClinicUser.CustomerInfo.Animals.FirstOrDefault(x => x.Id == createVisitModel.AnimalId));
            result.VisitTime.Should().Be(createVisitModel.VisitTime);
            result.IsVisitingHome.Should().Be(createVisitModel.IsVisitingHome);
        }

        [Fact]
        public async void create_visit_with_invalid_vet_should_throw_exception()
        {
            // arrange
            var vet = DatabaseContext.ArrangeApplicationUser(addRoles: false);
            var customer = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            await DatabaseContext.SaveChangesAsync();

            var createVisitModel = VisitArrangeExtension.CreateVisitModel(
                veterinarianId: vet.Id,
                customerId: customer.Id,
                animalId: customer.ClinicUser.CustomerInfo.Animals.FirstOrDefault()?.Id
                );

            // act
            Func<Task> act = () => _sut.CreateVisit(createVisitModel);

            // assert
            ArgumentException exception = await Assert.ThrowsAsync<ArgumentException>(act);
            exception.Message.Should().NotBeEmpty();
        }

        [Fact]
        public async void create_visit_with_invalid_animal_should_throw_exception()
        {
            // arrange
            var vet = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var customer = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            await DatabaseContext.SaveChangesAsync();

            var createVisitModel = VisitArrangeExtension.CreateVisitModel(
                veterinarianId: vet.Id,
                customerId: customer.Id,
                animalId: -1
                );

            // act
            Func<Task> act = () => _sut.CreateVisit(createVisitModel);

            // assert
            ArgumentException exception = await Assert.ThrowsAsync<ArgumentException>(act);
            exception.Message.Should().NotBeEmpty();
        }

        [Fact]
        public async void update_async_visit_should_be_saved()
        {
            // arrange
            var oldVet = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var newVet = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var customer = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var oldAnimal = customer.ClinicUser.CustomerInfo.Animals.FirstOrDefault();
            var newAnimal = customer.ClinicUser.CustomerInfo.Animals.Skip(1).FirstOrDefault();
            var visit = DatabaseContext.ArrangeVisit(oldVet, customer, oldAnimal);
            await DatabaseContext.SaveChangesAsync();


            var updateVisitModel = VisitArrangeExtension.CreateUpdateVisitModel(
                id: visit.Id,
                veterinarianId: newVet.Id,
                customerId: customer.Id,
                animalId: newAnimal.Id,
                veterinarianComment: "some veterinarian comment",
                customerComment: "some customer comment"
                );

            // act
            var result = await _sut.UpdateAsync(updateVisitModel);

            // assert
            result.Veterinarian.Should().BeEquivalentTo(newVet);
            result.Customer.Should().BeEquivalentTo(customer);
            result.Animal.Should().BeEquivalentTo(customer.ClinicUser.CustomerInfo.Animals.FirstOrDefault(x => x.Id == newAnimal.Id));
            result.VisitTime.Should().Be(updateVisitModel.VisitTime);
            result.IsVisitingHome.Should().Be(updateVisitModel.IsVisitingHome);
            result.VeterinarianComment.Should().Be(updateVisitModel.VeterinarianComment);
            result.CustomerComment.Should().Be(updateVisitModel.CustomerComment);
        }

        [Fact]
        public async void update_async_visit_with_invalid_vet_should_throw_exception()
        {
            // arrange
            var oldVet = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var newVet = DatabaseContext.ArrangeApplicationUser(addRoles: false);
            var customer = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var oldAnimal = customer.ClinicUser.CustomerInfo.Animals.FirstOrDefault();
            var newAnimal = customer.ClinicUser.CustomerInfo.Animals.Skip(1).FirstOrDefault();
            var visit = DatabaseContext.ArrangeVisit(oldVet, customer, oldAnimal);
            await DatabaseContext.SaveChangesAsync();


            var updateVisitModel = VisitArrangeExtension.CreateUpdateVisitModel(
                id: visit.Id,
                veterinarianId: newVet.Id,
                customerId: customer.Id,
                animalId: newAnimal.Id
                );

            // act
            Func<Task> act = () => _sut.UpdateAsync(updateVisitModel);

            // assert
            ArgumentException exception = await Assert.ThrowsAsync<ArgumentException>(act);
            exception.Message.Should().NotBeEmpty();
        }

        [Fact]
        public async void update_async_visit_with_invalid_animal_should_throw_exception()
        {
            // arrange
            var oldVet = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var newVet = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var customer = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var oldAnimal = customer.ClinicUser.CustomerInfo.Animals.FirstOrDefault();
            var visit = DatabaseContext.ArrangeVisit(oldVet, customer, oldAnimal);
            await DatabaseContext.SaveChangesAsync();


            var updateVisitModel = VisitArrangeExtension.CreateUpdateVisitModel(
                id: visit.Id,
                veterinarianId: newVet.Id,
                customerId: customer.Id,
                animalId: -1
                );

            // act
            Func<Task> act = () => _sut.UpdateAsync(updateVisitModel);

            // assert
            ArgumentException exception = await Assert.ThrowsAsync<ArgumentException>(act);
            exception.Message.Should().NotBeEmpty();
        }

        [Fact]
        public async void get_visits_filtered_by_veterinarian_id_and_date_from_should_return_correct_data()
        {
            // arrange
            var vet_1 = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var vet_2 = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var customer = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var animal_1 = customer.ClinicUser.CustomerInfo.Animals.First();
            var animal_2 = customer.ClinicUser.CustomerInfo.Animals.Skip(1).First();
            var animal_3 = customer.ClinicUser.CustomerInfo.Animals.Skip(2).First();
            var visit_1 = DatabaseContext.ArrangeVisit(vet_1, customer, animal_1, visitTime: new DateTime(2015, 10, 10));
            var visit_2 = DatabaseContext.ArrangeVisit(vet_1, customer, animal_2, visitTime: new DateTime(2025, 10, 10));
            var visit_3 = DatabaseContext.ArrangeVisit(vet_1, customer, animal_3, visitTime: new DateTime(2007, 10, 10));
            var visit_4 = DatabaseContext.ArrangeVisit(vet_2, customer, animal_1, visitTime: new DateTime(2070, 10, 10));
            var visit_5 = DatabaseContext.ArrangeVisit(vet_2, customer, animal_1, visitTime: new DateTime(1955, 10, 10));
            await DatabaseContext.SaveChangesAsync();

            VisitFilterModel filterModel = new VisitFilterModel()
            {
                Page = 0,
                PageSize = 10,
                VeterinarianId = vet_1.Id,
                DateFrom = new DateTime(2010, 10, 10)
            };

            // act
            var result = await _sut.GetVisitsByFilter(filterModel);

            // assert
            result.Should().NotBeNull();
            result.PageCount.Should().Be(1);
            result.RowCount.Should().Be(2);
            result.Results.Should().HaveCount(2);
            result.Results.Where(x => x.VeterinarianId == vet_1.Id).Should().HaveCount(2);
        }

        [Fact]
        public async void get_visits_filtered_by_animal_id_and_date_to_should_return_correct_data()
        {
            // arrange
            var vet_1 = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var vet_2 = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var customer = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var animal_1 = customer.ClinicUser.CustomerInfo.Animals.First();
            var animal_2 = customer.ClinicUser.CustomerInfo.Animals.Skip(1).First();
            var animal_3 = customer.ClinicUser.CustomerInfo.Animals.Skip(2).First();
            var visit_1 = DatabaseContext.ArrangeVisit(vet_1, customer, animal_1, visitTime: new DateTime(2015, 10, 10));
            var visit_2 = DatabaseContext.ArrangeVisit(vet_1, customer, animal_2, visitTime: new DateTime(2025, 10, 10));
            var visit_3 = DatabaseContext.ArrangeVisit(vet_1, customer, animal_3, visitTime: new DateTime(2007, 10, 10));
            var visit_4 = DatabaseContext.ArrangeVisit(vet_2, customer, animal_1, visitTime: new DateTime(2070, 10, 10));
            var visit_5 = DatabaseContext.ArrangeVisit(vet_2, customer, animal_1, visitTime: new DateTime(1955, 10, 10));
            await DatabaseContext.SaveChangesAsync();

            VisitFilterModel filterModel = new VisitFilterModel()
            {
                Page = 0,
                PageSize = 10,
                AnimalId = animal_1.Id,
                DateTo = new DateTime(2010, 10, 10)
            };

            // act
            var result = await _sut.GetVisitsByFilter(filterModel);

            // assert
            result.Should().NotBeNull();
            result.PageCount.Should().Be(1);
            result.RowCount.Should().Be(1);
            result.Results.Should().HaveCount(1);
            result.Results.Where(x => x.AnimalId == animal_1.Id).Should().HaveCount(1);
        }
    }
}
