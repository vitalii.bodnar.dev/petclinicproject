﻿using Bogus;
using PetClinic.Application.Dtos.Users.Create;

namespace PetClinic.Tests.Infrastructure.ModelArrangeExtensions
{
    public static class UserDtoArrangeExtension
    {
        public static CreateApplicationUserDto CreateUserDto(
            string email = null,
            string password = null,
            string firstName = null,
            string lastName = null)
        {
            var faker = new Faker<CreateApplicationUserDto>()
                .RuleFor(x => x.Email, g => string.IsNullOrEmpty(email) ? g.Internet.Email() : email)
                .RuleFor(x => x.Password, g => string.IsNullOrEmpty(password) ? g.Internet.Password() : password)
                .RuleFor(x => x.FirstName, g => string.IsNullOrEmpty(firstName) ? g.Name.FirstName() : firstName)
                .RuleFor(x => x.LastName, g => string.IsNullOrEmpty(lastName) ? g.Name.LastName() : lastName);

            return faker.Generate();
        }
    }
}
