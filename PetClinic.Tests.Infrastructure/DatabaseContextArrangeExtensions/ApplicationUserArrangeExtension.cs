﻿using Bogus;
using PetClinic.DbMigration.DataAccess;
using PetClinic.Domain.AggregateModels;
using PetClinic.Domain.AggregateModels.Animals;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.ApplicationUserModels;
using PetClinic.Domain.Enums;
using System;
using System.Collections.Generic;

namespace PetClinic.Tests.Infrastructure.DatabaseContextArrangeExtensions
{
    public static class ApplicationUserArrangeExtension
    {
        public static ApplicationUser ArrangeApplicationUser(
            this ApplicationDbContext dbContext,
            Guid? id = null,
            string email = null,
            string userName = null,
            string passwordHash = null,
            string passwordSalt = null,
            string phoneNumber = null,
            bool? addRoles = null)
        {
            var applicationUser = CreateApplicationUser(id, email, userName, passwordHash, passwordSalt, phoneNumber);

            if (addRoles.HasValue && addRoles.Value)
            {
                var role1 = dbContext.ArrangeApplicationRole(name: Roles.CUSTOMER);
                var role2 = dbContext.ArrangeApplicationRole(name: Roles.VETERINARIAN);
                dbContext.SaveChanges();

                applicationUser.Roles.Add(role1);
                applicationUser.Roles.Add(role2);
            }

            dbContext.ApplicationUsers.Add(applicationUser);
            return applicationUser;
        }

        public static ApplicationUser CreateApplicationUser(
            Guid? id = null,
            string email = null,
            string userName = null,
            string passwordHash = null,
            string passwordSalt = null,
            string phoneNumber = null)
        {
            var faker = new Faker<ApplicationUser>()
                .RuleFor(x => x.Id, g => id ?? g.Database.Random.Guid())
                .RuleFor(x => x.Email, g => string.IsNullOrEmpty(email) ? g.Internet.Email() : email)
                .RuleFor(x => x.UserName, g => string.IsNullOrEmpty(userName) ? g.Random.String(99) : userName)
                .RuleFor(x => x.ClinicUser, g => CreateClinicUser())
                .RuleFor(x => x.PasswordHash, g => string.IsNullOrEmpty(passwordHash) ? g.Random.String(250) : passwordHash)
                .RuleFor(x => x.PasswordSalt, g => string.IsNullOrEmpty(passwordSalt) ? g.Random.String(250) : passwordSalt)
                .RuleFor(x => x.PhoneNumber, g => string.IsNullOrEmpty(phoneNumber) ? g.Random.String(10) : phoneNumber);

            return faker.Generate();
        }

        public static ApplicationRole ArrangeApplicationRole(
            this ApplicationDbContext dbContext,
            Guid? id = null,
            string name = null,
            string description = null)
        {

            var applicationRole = CreateApplicationRole(id, name, description);
            dbContext.ApplicationRoles.Add(applicationRole);
            return applicationRole;
        }

        public static ApplicationRole CreateApplicationRole(
            Guid? id = null,
            string name = null,
            string description = null)
        {
            var faker = new Faker<ApplicationRole>()
                .RuleFor(x => x.Id, g => id ?? g.Database.Random.Guid())
                .RuleFor(x => x.Name, g => string.IsNullOrEmpty(name) ? g.Random.String(99) : name)
                .RuleFor(x => x.Description, g => string.IsNullOrEmpty(description) ? g.Random.String(249) : description);

            var role = faker.Generate();
            role.NormalizedName = role.Name.ToLower();
            return role;
        }

        public static ClinicUser CreateClinicUser(
            Guid? id = null,
            string firstName = null,
            string secondName = null,
            string lastName = null
            )
        {
            var faker = new Faker<ClinicUser>()
                .RuleFor(x => x.Id, g => id ?? g.Database.Random.Guid())
                .RuleFor(x => x.FirstName, g => string.IsNullOrEmpty(firstName) ? g.Name.FirstName() : firstName)
                .RuleFor(x => x.SecondName, g => string.IsNullOrEmpty(secondName) ? g.Name.LastName() : secondName)
                .RuleFor(x => x.LastName, g => string.IsNullOrEmpty(lastName) ? g.Name.LastName() : lastName)
                .RuleFor(x => x.CustomerInfo, g => CreateCustomerInfo())
                .RuleFor(x => x.VetInfo, g => CreateVetInfo());

            return faker.Generate();
        }

        public static VetInfo CreateVetInfo(
            int? id = null,
            string description = null
            )
        {
            var faker = new Faker<VetInfo>()
                .RuleFor(x => x.Id, g => id.GetValueOrDefault())
                .RuleFor(x => x.Description, g => string.IsNullOrEmpty(description) ? g.Random.String(250) : description)
                .RuleFor(x => x.ActiveLicense, g => CreateVetLicense());

            return faker.Generate();
        }

        public static VetLicense CreateVetLicense(
            int? id = null,
            string number = null,
            string issuingAuthorityName = null,
            string issuingAuthorityId = null,
            DateTime? dateOfIssue = null,
            DateTime? dateOfExpiry = null
            )
        {
            var faker = new Faker<VetLicense>()
                .RuleFor(x => x.Id, g => id.GetValueOrDefault())
                .RuleFor(x => x.Number, g => string.IsNullOrEmpty(number) ? g.Random.String(49) : number)
                .RuleFor(x => x.IssuingAuthorityName, g => string.IsNullOrEmpty(issuingAuthorityName) ? g.Random.String(199) : issuingAuthorityName)
                .RuleFor(x => x.IssuingAuthorityId, g => string.IsNullOrEmpty(issuingAuthorityId) ? g.Random.String(49) : issuingAuthorityId)
                .RuleFor(x => x.DateOfIssue, g => dateOfIssue ?? g.Date.Past())
                .RuleFor(x => x.DateOfExpiry, g => dateOfExpiry ?? g.Date.Past());

            return faker.Generate();
        }

        public static CustomerInfo CreateCustomerInfo(
            int? id = null)
        {
            var faker = new Faker<CustomerInfo>()
                .RuleFor(x => x.Id, g => id.GetValueOrDefault())
                .RuleFor(x => x.Animals, g => new List<Animal>()
                {
                    CreateAnimal(),
                    CreateAnimal(),
                    CreateAnimal()
                });

            var customerInfo = faker.Generate();
            return customerInfo;
        }

        public static Animal CreateAnimal(
            int? id = null,
            string name = null,
            string description = null,
            DateTime? dateOfBirth = null,
            AnimalType? animalType = null
            )
        {
            var faker = new Faker<Animal>()
                .RuleFor(x => x.Id, g => id.GetValueOrDefault())
                .RuleFor(x => x.Name, g => string.IsNullOrEmpty(name) ? g.Random.String(49) : name)
                .RuleFor(x => x.Description, g => string.IsNullOrEmpty(description) ? g.Random.String(250) : description)
                .RuleFor(x => x.DateOfBirth, g => dateOfBirth ?? g.Date.Past())
                .RuleFor(x => x.Type, g => animalType ?? g.Random.Enum<AnimalType>())
                .RuleFor(x => x.Vaccinations, g => new List<Vaccination>() 
                {
                    CreateVaccination(),
                    CreateVaccination()
                });

            return faker.Generate();
        }

        public static Vaccination CreateVaccination(
            int? id = null,
            string name = null,
            string code = null,
            string description = null
            )
        {
            var faker = new Faker<Vaccination>()
                .RuleFor(x => x.Id, g => id.GetValueOrDefault())
                .RuleFor(x => x.Name, g => string.IsNullOrEmpty(name) ? g.Random.String(49) : name)
                .RuleFor(x => x.Code, g => string.IsNullOrEmpty(code) ? g.Random.String(49) : code)
                .RuleFor(x => x.Description, g => string.IsNullOrEmpty(description) ? g.Random.String(250) : description);

            return faker.Generate();
        }

        public static string GenerateEmail()
        {
            return new Faker().Internet.Email();
        }
    }
}
