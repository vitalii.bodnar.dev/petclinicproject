﻿using Bogus;
using PetClinic.DbMigration.DataAccess;
using PetClinic.Domain.AggregateModels;
using PetClinic.Domain.AggregateModels.Animals;
using PetClinic.Domain.ApplicationUserModels;
using PetClinic.Domain.Models;
using System;

namespace PetClinic.Tests.Infrastructure.DatabaseContextArrangeExtensions
{
    public static class VisitArrangeExtension
    {
        public static Visit ArrangeVisit(
            this ApplicationDbContext dbContext,
            ApplicationUser veterinarian,
            ApplicationUser customer,
            Animal animal,
            DateTime? visitTime = null,
            bool? isVisitingHome = null,
            string veterinarianComment = null,
            string customerComment = null
            )
        {
            var visit = CreateVisit(veterinarian,
                customer,
                animal,
                visitTime,
                isVisitingHome,
                veterinarianComment,
                customerComment);

            dbContext.Visits.Add(visit);
            return visit;
        }

        public static Visit CreateVisit(
            ApplicationUser veterinarian,
            ApplicationUser customer,
            Animal animal,
            DateTime? visitTime = null,
            bool? isVisitingHome = null,
            string veterinarianComment = null,
            string customerComment = null
            )
        {
            var faker = new Faker<Visit>()
                .RuleFor(x => x.Veterinarian, g => veterinarian)
                .RuleFor(x => x.Customer, g => customer)
                .RuleFor(x => x.Animal, g => animal)
                .RuleFor(x => x.VisitTime, g => visitTime ?? g.Date.Past())
                .RuleFor(x => x.IsVisitingHome, g => isVisitingHome ?? g.Random.Bool())
                .RuleFor(x => x.VeterinarianComment, g => veterinarianComment ?? g.Random.String(500))
                .RuleFor(x => x.CustomerComment, g => customerComment ?? g.Random.String(500));

            return faker.Generate();
        }

        public static CreateVisitModel CreateVisitModel(
            Guid? veterinarianId = null,
            Guid? customerId = null,
            int? animalId = null,
            DateTime? visitTime = null,
            bool? isVisitingHome = null
            )
        {
            var faker = new Faker<CreateVisitModel>()
                .RuleFor(x => x.VeterinarianId, g => veterinarianId ?? g.Random.Guid())
                .RuleFor(x => x.CustomerId, g => customerId ?? g.Random.Guid())
                .RuleFor(x => x.AnimalId, g => animalId ?? g.Random.Int(1, 50))
                .RuleFor(x => x.VisitTime, g => visitTime ?? g.Date.Past())
                .RuleFor(x => x.IsVisitingHome, g => isVisitingHome ?? g.Random.Bool());

            return faker.Generate();
        }

        public static UpdateVisitModel CreateUpdateVisitModel(
            int? id = null,
            Guid? veterinarianId = null,
            Guid? customerId = null,
            int? animalId = null,
            DateTime? visitTime = null,
            bool? isVisitingHome = null,
            string veterinarianComment = null,
            string customerComment = null
            )
        {
            var faker = new Faker<UpdateVisitModel>()
                .RuleFor(x => x.Id, g => id.GetValueOrDefault())
                .RuleFor(x => x.VeterinarianId, g => veterinarianId ?? g.Random.Guid())
                .RuleFor(x => x.CustomerId, g => customerId ?? g.Random.Guid())
                .RuleFor(x => x.AnimalId, g => animalId ?? g.Random.Int(1, 50))
                .RuleFor(x => x.VisitTime, g => visitTime ?? g.Date.Past())
                .RuleFor(x => x.IsVisitingHome, g => isVisitingHome ?? g.Random.Bool())
                .RuleFor(x => x.VeterinarianComment, g => veterinarianComment ?? g.Random.String(500))
                .RuleFor(x => x.CustomerComment, g => customerComment ?? g.Random.String(500));

            return faker.Generate();
        }
    }
}
