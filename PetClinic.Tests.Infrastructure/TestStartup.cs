﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using PetClinic.Api;
using PetClinic.DbMigration.DataAccess;
using System.Diagnostics;
using System.IO;

namespace PetClinic.Tests.Infrastructure
{

    public class TestStartup : Startup
    {
        public TestStartup() : base(new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.tests.json", optional: false, reloadOnChange: true)
            .Build())
        {

        }
        protected override void ConfigureDataAccessLayer(IServiceCollection services, IConfiguration configuration)
        {
            string connectionString = configuration.GetSection("ConnectionString")["Default"];
            Debug.Assert(connectionString != null && connectionString != "TestConnectionString", "Expected: connectionString != null");
            services
                .AddEntityFrameworkSqlServer()
                .AddDbContext<ApplicationDbContext>(options =>
                {
                    options.UseSqlServer(connectionString);
                    options.UseOpenIddict();
                }, ServiceLifetime.Singleton);

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }
        
        protected override void ConfigureAuthentication(IServiceCollection services, IConfiguration configuration)
        {
            // it is empty in order to provide configuration from the fake token provider
        }
    }
}
