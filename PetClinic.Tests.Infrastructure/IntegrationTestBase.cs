﻿using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.ApplicationParts;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using Microsoft.AspNetCore.TestHost;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer.Storage.Internal;
using Microsoft.Extensions.DependencyInjection;
using PetClinic.Api;
using PetClinic.DbMigration.DataAccess;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using Serilog;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using WebMotions.Fake.Authentication.JwtBearer;
using WebMotions.Fake.Authentication.JwtBearer.Events;
using Xunit;

namespace PetClinic.Tests.Infrastructure
{
    public abstract class IntegrationTestBase<TStartup> : IClassFixture<TStartup>, IDisposable where TStartup : class
    {
        private string _testUserName = "TestUser";

        protected readonly TestServer Server;

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Usage", "EF1001:Internal EF Core API usage.", Justification = "<Pending>")]
        private SqlServerTransaction _dbContextTransaction;

        protected HttpClient Client { get; }

        protected ApplicationDbContext DatabaseContext { get; }

        public IntegrationTestBase()
        {
            var webHostBuilder = new WebHostBuilder()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .ConfigureServices(InitializeServices)
                .UseEnvironment("Development")
                .UseSerilog()
                .UseStartup(typeof(TStartup));

            Server = new TestServer(webHostBuilder);
            Client = Server.CreateClient();

            SetFakeClientBearerToken();
            DatabaseContext = GetService<ApplicationDbContext>();
            DatabaseContext.Database.EnsureCreated();
            CreateTransaction();
        }

        protected virtual void InitializeServices(IServiceCollection services)
        {
            var startupAssembly = typeof(Startup).GetTypeInfo().Assembly;

            var manager = new ApplicationPartManager
            {
                ApplicationParts =
                {
                    new AssemblyPart(startupAssembly)
                },
                FeatureProviders =
                {
                    new ControllerFeatureProvider(),
                    new ViewComponentFeatureProvider()
                }
            };

            services.AddAutofac();
            services.AddAuthentication(FakeJwtBearerDefaults.AuthenticationScheme)
                .AddFakeJwtBearer(opt =>
                {
                    opt.Events = new JwtBearerEvents
                    {
                        OnChallenge = async context =>
                        {
                            context.HandleResponse();
                            await Task.CompletedTask;
                        }
                    };
                });

            services.AddSingleton(manager);
        }

        private void CreateTransaction()
        {
            _dbContextTransaction = (SqlServerTransaction)DatabaseContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadCommitted);
        }

        protected void SetFakeClientBearerToken()
        {
            Client.SetFakeBearerToken(_testUserName, new string[] { Roles.ADMINISTRATOR });
        }

        public T GetService<T>() => (T)Server.Services.GetService(typeof(T));

        public void Dispose()
        {
            _dbContextTransaction.Dispose();
        }
    }
}
