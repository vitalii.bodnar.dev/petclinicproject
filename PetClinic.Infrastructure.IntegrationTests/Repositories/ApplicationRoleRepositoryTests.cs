﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.ApplicationUserModels;
using PetClinic.Infrastructure.Repositories;
using PetClinic.Tests.Infrastructure;
using PetClinic.Tests.Infrastructure.DatabaseContextArrangeExtensions;
using System.Linq;
using Xunit;

namespace PetClinic.Infrastructure.IntegrationTests.Repositories
{
    public class ApplicationRoleRepositoryTests : IntegrationTestBase<TestStartup>
    {
        private readonly ApplicationRoleRepository _sut;
        public ApplicationRoleRepositoryTests()
        {
            var logger = Mock.Of<ILogger<ApplicationRoleRepository>>();
            _sut = new ApplicationRoleRepository(DatabaseContext, logger);
        }

        [Fact]
        public async void add_application_role_should_be_saved()
        {
            // arrange
            var applicationRole = ApplicationUserArrangeExtension.CreateApplicationRole();

            // act
            await _sut.Add(applicationRole);

            // assert
            var result = DatabaseContext.Find<ApplicationRole>(applicationRole.Id);
            result.Should().BeEquivalentTo(applicationRole);
        }

        [Fact]
        public void update_application_role_should_save_role_data()
        {
            // arrange
            var applicationRole = DatabaseContext.ArrangeApplicationRole();
            DatabaseContext.SaveChanges();
            applicationRole.Description = "Some test description";

            // act
            _sut.Update(applicationRole);

            // assert
            var result = DatabaseContext.ApplicationRoles.Find(applicationRole.Id);
            result.Should().BeEquivalentTo(applicationRole);
        }

        [Fact]
        public async void get_role_by_name_should_return_correct_role()
        {
            // arrange
            var applicationRole = DatabaseContext.ArrangeApplicationRole();
            DatabaseContext.SaveChanges();

            // act
            var result = await _sut.GetRoleByName(applicationRole.Name);

            // assert
            result.Should().BeEquivalentTo(applicationRole);
        }

        [Fact]
        public void init_roles_with_no_roles_should_create_base_roles()
        {
            // arrange
            // lack

            // act
            _sut.InitRoles();

            // assert
            var result = DatabaseContext.ApplicationRoles.ToList();
            result.Should().HaveCount(3);
            result.First(x => x.Name == Roles.CUSTOMER).Should().NotBeNull();
            result.First(x => x.Name == Roles.VETERINARIAN).Should().NotBeNull();
            result.First(x => x.Name == Roles.ADMINISTRATOR).Should().NotBeNull();
        }

        [Fact]
        public void init_roles_with_roles_should_not_create_base_roles()
        {
            // arrange
            var roleName = "single role in application";
            var applicationRole = ApplicationUserArrangeExtension.CreateApplicationRole(name: roleName);
            DatabaseContext.Add(applicationRole);
            DatabaseContext.SaveChanges();

            // act
            _sut.InitRoles();

            // assert
            var result = DatabaseContext.ApplicationRoles.ToList();
            result.Should().HaveCount(1);
            result.First(x => x.Name == roleName).Should().NotBeNull();
        }
    }
}
