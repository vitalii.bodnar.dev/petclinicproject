﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using PetClinic.Domain.AggregateModels.Animals;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.ApplicationUserModels;
using PetClinic.Infrastructure.Repositories;
using PetClinic.Tests.Infrastructure;
using PetClinic.Tests.Infrastructure.DatabaseContextArrangeExtensions;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace PetClinic.Infrastructure.IntegrationTests.Repositories
{
    public class ApplicationUserRepositoryTests : IntegrationTestBase<TestStartup>
    {
        private readonly ApplicationUserRepository _sut;

        public ApplicationUserRepositoryTests()
        { 
            var logger = Mock.Of<ILogger<ApplicationUserRepository>>();
            _sut = new ApplicationUserRepository(DatabaseContext, logger);
        }

        [Fact]
        public async void add_application_user_should_be_saved()
        {
            // arrange
            var applicationUser = ApplicationUserArrangeExtension.CreateApplicationUser();

            // act
            await _sut.Add(applicationUser);

            // assert
            var result = DatabaseContext.Find<ApplicationUser>(applicationUser.Id);
            result.Should().BeEquivalentTo(applicationUser);
        }

        [Fact]
        public async void get_async_should_return_correct_user()
        {
            // arrange
            var applicationUser = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            DatabaseContext.SaveChanges();

            // act
            var result = await _sut.GetAsync(applicationUser.Id);

            // assert
            result.Should().BeEquivalentTo(applicationUser);
        }

        [Fact]
        public async void find_by_email_async_should_return_correct_user()
        {
            // arrange
            var applicationUser = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            DatabaseContext.SaveChanges();

            // act
            var result = await _sut.FindByEmailAsync(applicationUser.Email);

            // assert
            result.Should().BeEquivalentTo(applicationUser);
        }

        [Fact]
        public async void get_customers_should_return_correct_user()
        {
            // arrange
            var applicationUser = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            DatabaseContext.SaveChanges();

            // act
            var result = await _sut.GetCustomers();

            // assert
            result.Should().HaveCount(1);
            result.First().Should().BeEquivalentTo(applicationUser);
        }

        [Fact]
        public async void get_veterinarians_should_return_correct_user()
        {
            // arrange
            var applicationUser = ApplicationUserArrangeExtension.CreateApplicationUser();
            var role = ApplicationUserArrangeExtension.CreateApplicationRole(name: Roles.VETERINARIAN);
            await DatabaseContext.AddAsync(role);
            applicationUser.AddRoleToUser(role);
            await DatabaseContext.AddAsync(applicationUser);
            await DatabaseContext.SaveChangesAsync();

            // act
            var result = await _sut.GetVeterinarians();

            // assert
            result.Should().HaveCount(1);
            result.First().Should().BeEquivalentTo(applicationUser);
        }

        [Fact]
        public async void update_async_should_save_user_data()
        {
            // arrange
            var applicationUser = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            DatabaseContext.SaveChanges();
            applicationUser.PhoneNumber = "PhoneNumber";
            applicationUser.ClinicUser.FirstName = "FirstName";
            applicationUser.ClinicUser.CustomerInfo.Animals = new List<Animal>()
            {
                ApplicationUserArrangeExtension.CreateAnimal()
            };

            applicationUser.ClinicUser.VetInfo.Description = "Some test description";
            applicationUser.ClinicUser.VetInfo.ActiveLicense.IssuingAuthorityName = "Some test issuing authority name";

            // act
            await _sut.UpdateAsync(applicationUser);

            // assert
            var result = DatabaseContext.Find<ApplicationUser>(applicationUser.Id);
            result.Should().BeEquivalentTo(applicationUser);
        }

        [Fact]
        public void is_email_unique_should_return_true_for_unique_email()
        {
            // arrange
            var uniqueEmail = ApplicationUserArrangeExtension.GenerateEmail();

            // act
            var result = _sut.IsEmailUnique(uniqueEmail);

            // assert
            result.Should().BeTrue();
        }

        [Fact]
        public void is_email_unique_should_return_false_for_nonunique_email()
        {
            // arrange
            var applicationUser = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            DatabaseContext.SaveChanges();

            // act
            var result = _sut.IsEmailUnique(applicationUser.Email);

            // assert
            result.Should().BeFalse();
        }

        [Fact]
        public void is_email_unique_should_return_true_for_unique_email_of_current_user()
        {
            // arrange
            var applicationUser = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            DatabaseContext.SaveChanges();

            // act
            var result = _sut.IsEmailUnique(applicationUser.Email, applicationUser.Id);

            // assert
            result.Should().BeTrue();
        }
    }
}
