﻿using PetClinic.Domain.ApplicationUserModels;
using System;
using System.Web.Helpers;

namespace PetClinic.Service.Helpers
{
    public static class PasswordHelper
    {
        public static void GenerateHashPassword(this ApplicationUser user, string password)
        {
            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("Password is null or empty.");
            }

            var salt = Crypto.GenerateSalt();
            user.PasswordSalt = salt;
            user.PasswordHash = Crypto.HashPassword(password + salt);
        }

        public static bool VerifyPassword(this ApplicationUser user, string password)
        {
            return Crypto.VerifyHashedPassword(user.PasswordHash, password + user.PasswordSalt);
        }
    }
}
