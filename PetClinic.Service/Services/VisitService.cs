﻿using Microsoft.Extensions.Logging;
using PetClinic.Domain;
using PetClinic.Domain.AggregateModels;
using PetClinic.Domain.Models;
using PetClinic.Infrastructure.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Service.Services
{
    public class VisitService : BaseEntityService<IVisitRepository, Visit, int>, IVisitService
    {
        private readonly IApplicationUserRepository _userRepository;

        public VisitService(IVisitRepository repository, IApplicationUserRepository userRepository, ILogger<VisitService> logger)
            : base(repository, logger)
        {
            _userRepository = userRepository;
        }

        public async Task<Visit> CreateVisit(CreateVisitModel model, CancellationToken cancellationToken = default)
        {
            var customer = await _userRepository.GetAsync(model.CustomerId, cancellationToken);
            var veterinarian = await _userRepository.GetAsync(model.VeterinarianId, cancellationToken);
            var animal = customer?.ClinicUser?.CustomerInfo?.Animals.FirstOrDefault(x => x.Id == model.AnimalId);
            var visit = new Visit(veterinarian, customer, animal)
            {
                VisitTime = model.VisitTime,
                IsVisitingHome = model.IsVisitingHome
            };

            await Repository.Add(visit, cancellationToken);
            return visit;
        }

        public async Task<Visit> UpdateAsync(UpdateVisitModel model, CancellationToken cancellationToken = default)
        {
            var visit = await Repository.GetAsync(model.Id, cancellationToken);
            var customer = await _userRepository.GetAsync(model.CustomerId, cancellationToken);
            var veterinarian = await _userRepository.GetAsync(model.VeterinarianId, cancellationToken);
            var animal = customer?.ClinicUser?.CustomerInfo?.Animals.FirstOrDefault(x => x.Id == model.AnimalId);

            visit.SetVeterinarian(veterinarian);
            visit.SetCustomerWithAnimal(customer, animal);

            visit.IsVisitingHome = model.IsVisitingHome;
            visit.VisitTime = model.VisitTime;
            visit.VeterinarianComment = model.VeterinarianComment;
            visit.CustomerComment = model.CustomerComment;

            await Repository.UpdateAsync(visit, cancellationToken);
            return visit;
        }

        public async Task<PagedResult<Visit>> GetVisitsByFilter(VisitFilterModel visitFilterModel)
        {
            return await Repository.GetVisitsByFilter(visitFilterModel);
        }
    }
}
