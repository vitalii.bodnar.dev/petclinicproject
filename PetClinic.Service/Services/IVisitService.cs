﻿using PetClinic.Domain;
using PetClinic.Domain.AggregateModels;
using PetClinic.Domain.Models;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Service.Services
{
    public interface IVisitService : IBaseEntityService<Visit, int>
    {
        Task<Visit> CreateVisit(CreateVisitModel model, CancellationToken cancellationToken = default);
        Task<Visit> UpdateAsync(UpdateVisitModel updateVisitModel, CancellationToken cancellationToken = default);
        Task<PagedResult<Visit>> GetVisitsByFilter(VisitFilterModel visitFilterModel);
    }
}
