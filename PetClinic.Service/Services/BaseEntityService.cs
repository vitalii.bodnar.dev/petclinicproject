﻿using Microsoft.Extensions.Logging;
using PetClinic.Domain.AggregateModels;
using PetClinic.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Service.Services
{
    public abstract class BaseEntityService<TRepository, TEntity, T> : IBaseEntityService<TEntity, T>
        where TRepository : IBaseEntityRepository<TEntity, T>
        where TEntity : class, IEntity<T>
    {
        protected readonly ILogger<BaseEntityService<TRepository, TEntity, T>> Logger;
        protected readonly TRepository Repository;
        public BaseEntityService(TRepository repository, ILogger<BaseEntityService<TRepository, TEntity, T>> logger)
        {
            Repository = repository;
            Logger = logger;
            if (Repository == null)
            {
                Logger.LogError("BaseEntityService Repository is null");
                throw new ArgumentException(nameof(repository));
            }
        }

        public async Task Add(TEntity entity, CancellationToken cancellationToken = default)
        {
            await Repository.Add(entity, cancellationToken);
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return Repository.Find(predicate);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return Repository.GetAll();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            return await Repository.GetAllAsync(cancellationToken);
        }

        public async Task<TEntity> GetAsync(T id, CancellationToken cancellationToken = default)
        {
            return await Repository.GetAsync(id, cancellationToken);
        }

        public void Remove(TEntity entity)
        {
            Repository.Remove(entity);
        }

        public void Update(TEntity entity)
        {
            Repository.Update(entity);
        }

        public async Task UpdateAsync(TEntity entity, CancellationToken cancellationToken = default)
        {
            await Repository.UpdateAsync(entity, cancellationToken);
        }
    }
}
