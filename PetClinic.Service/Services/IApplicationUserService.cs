﻿using PetClinic.Domain.ApplicationUserModels;
using PetClinic.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Service.Services
{
    public interface IApplicationUserService : IBaseEntityService<ApplicationUser, Guid>
    {
        Task<ApplicationUser> FindByEmailAsync(string email, CancellationToken cancellationToken = default);
        bool CheckPassword(ApplicationUser user, string password);
        bool IsEmailUnique(string email, Guid? id = null);
        bool IsPasswordValid(string password);
        Task CreateCustomer(CreateUserModel createUserModel, CancellationToken cancellationToken = default);
        Task CreateVeterinarian(CreateUserModel createUserModel, CancellationToken cancellationToken = default);
        Task<IEnumerable<ApplicationUser>> GetCustomers(CancellationToken cancellationToken = default);
        Task<IEnumerable<ApplicationUser>> GetVeterinarians(CancellationToken cancellationToken = default);
    }
}
