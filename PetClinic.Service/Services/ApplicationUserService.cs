﻿using Microsoft.Extensions.Logging;
using PetClinic.Domain.AggregateModels;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.ApplicationUserModels;
using PetClinic.Domain.Models;
using PetClinic.Infrastructure.Repositories;
using PetClinic.Service.Helpers;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Service.Services
{
    public class ApplicationUserService : BaseEntityService<IApplicationUserRepository, ApplicationUser, Guid>, IApplicationUserService
    {
        private readonly IApplicationRoleRepository _roleRepository;

        public ApplicationUserService(IApplicationUserRepository repository, IApplicationRoleRepository roleRepository, ILogger<ApplicationUserService> logger)
            : base(repository, logger)
        {
            _roleRepository = roleRepository;
        }

        public bool CheckPassword(ApplicationUser user, string password)
        {
            try
            {
                return user.VerifyPassword(password);
            }
            catch (Exception ex)
            {
                Logger.LogWarning($"user id: {user.Id} CheckPassword Exception: ", ex);
                return false;
            }
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email, CancellationToken cancellationToken = default)
        {
            return await Repository.FindByEmailAsync(email, cancellationToken);
        }

        public async Task CreateCustomer(CreateUserModel createUserModel, CancellationToken cancellationToken = default)
        {
            var role = await _roleRepository.GetRoleByName(Roles.CUSTOMER);
            var applicationUser = await CreateUser(createUserModel, new List<ApplicationRole>() { role });

            await Repository.Add(applicationUser, cancellationToken);
        }

        public async Task CreateVeterinarian(CreateUserModel createUserModel, CancellationToken cancellationToken = default)
        {
            var role = await _roleRepository.GetRoleByName(Roles.VETERINARIAN);
            var applicationUser = await CreateUser(createUserModel, new List<ApplicationRole>() { role });

            await Repository.Add(applicationUser, cancellationToken);
        }

        public bool IsEmailUnique(string email, Guid? id = null)
        {
            return Repository.IsEmailUnique(email, id);
        }

        public bool IsPasswordValid(string password)
        {
            var regularExpression = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$";
            var regex = new Regex(regularExpression);
            return regex.IsMatch(password);
        }

        private Task<ApplicationUser> CreateUser(CreateUserModel createUserModel, IEnumerable<ApplicationRole> roles)
        {
            var clinicUser = new ClinicUser()
            {
                FirstName = createUserModel.FirstName,
                LastName = createUserModel.LastName
            };

            var applicationUser = new ApplicationUser()
            {
                UserName = createUserModel.Email,
                Email = createUserModel.Email,
                NormalizedEmail = createUserModel.Email.ToLower(),
                PhoneNumber = "-",
                ClinicUser = clinicUser
            };
            applicationUser.GenerateHashPassword(createUserModel.Password);
            applicationUser.AddRolesToUser(roles);
            return Task.FromResult(applicationUser);
        }

        public async Task<IEnumerable<ApplicationUser>> GetCustomers(CancellationToken cancellationToken = default)
        {
            return await Repository.GetCustomers(cancellationToken);
        }

        public async Task<IEnumerable<ApplicationUser>> GetVeterinarians(CancellationToken cancellationToken = default)
        {
            return await Repository.GetVeterinarians(cancellationToken);
        }
    }
}
