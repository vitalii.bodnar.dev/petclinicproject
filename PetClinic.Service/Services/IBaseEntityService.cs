﻿using PetClinic.Domain.AggregateModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Service.Services
{
    public interface IBaseEntityService<TEntity, T>
        where TEntity : class, IEntity<T>
    {
        Task<TEntity> GetAsync(T id, CancellationToken cancellationToken = default);
        IEnumerable<TEntity> GetAll();
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        Task Add(TEntity entity, CancellationToken cancellationToken = default);
        void Remove(TEntity entity);
        void Update(TEntity entity);
        Task UpdateAsync(TEntity entity, CancellationToken cancellationToken = default);
        Task<IEnumerable<TEntity>> GetAllAsync(CancellationToken cancellationToken = default);
    }
}