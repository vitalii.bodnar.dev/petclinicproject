﻿using Microsoft.EntityFrameworkCore;
using PetClinic.Domain;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PetClinic.Infrastructure.Extensions
{
    public static class PagedResultExtension
    {
        public static async Task<PagedResult<T>> GetPaged<T>(this IQueryable<T> query,
                                         int page, int pageSize) where T : class
        {
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = pageSize,
                RowCount = query.Count()
            };

            var pageCount = (double)result.RowCount / pageSize;
            result.PageCount = (int)Math.Ceiling(pageCount);

            var skip = page < 0 ? (page - 1) * pageSize : page * pageSize;
            result.Results = await query.Skip(skip).Take(pageSize).ToListAsync();

            return result;
        }
    }
}
