﻿using PetClinic.Domain.AggregateModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Infrastructure.Repositories
{
    public interface IBaseEntityRepository<T, TEntity> : IDisposable
        where T: class, IEntity<TEntity>
    {
        Task<T> GetAsync(TEntity id, CancellationToken cancellationToken = default);
        IEnumerable<T> GetAll();
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        Task Add(T entity, CancellationToken cancellationToken = default);
        void Remove(T entity);
        void Update(T entity);
        Task UpdateAsync(T entity, CancellationToken cancellationToken = default);
        Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken = default);
    }
}
