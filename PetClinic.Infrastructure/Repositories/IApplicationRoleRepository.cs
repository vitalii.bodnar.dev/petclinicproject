﻿using PetClinic.Domain.ApplicationUserModels;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Infrastructure.Repositories
{
    public interface IApplicationRoleRepository : IBaseEntityRepository<ApplicationRole, Guid>
    {
        Task<ApplicationRole> GetRoleByName(string roleName, CancellationToken cancellationToken = default);
        void InitRoles();
    }
}
