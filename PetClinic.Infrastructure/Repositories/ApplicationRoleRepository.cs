﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PetClinic.DbMigration.DataAccess;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.ApplicationUserModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Infrastructure.Repositories
{
    public class ApplicationRoleRepository : BaseEntityRepository<ApplicationRole, Guid>, IApplicationRoleRepository
    {
        public ApplicationRoleRepository(ApplicationDbContext context, ILogger<ApplicationRoleRepository> logger) : base(context, logger) { }

        public async Task<ApplicationRole> GetRoleByName(string roleName, CancellationToken cancellationToken = default)
        {
            return await DbContext.ApplicationRoles
                .FirstOrDefaultAsync(x => x.NormalizedName == roleName.ToLower(), cancellationToken);
        }

        public void InitRoles()
        {
            if (DbContext.ApplicationRoles.Any())
            {
                return;
            }

            DbContext.ApplicationRoles.Add(new ApplicationRole()
            {
                Name = Roles.CUSTOMER,
                NormalizedName = Roles.CUSTOMER.ToLower(),
                Description = "Customer's role"
            });

            DbContext.ApplicationRoles.Add(new ApplicationRole()
            {
                Name = Roles.VETERINARIAN,
                NormalizedName = Roles.VETERINARIAN.ToLower(),
                Description = "Veterinarian's role. Give access to visits, patient history and medical records."
            });

            DbContext.ApplicationRoles.Add(new ApplicationRole()
            {
                Name = Roles.ADMINISTRATOR,
                NormalizedName = Roles.ADMINISTRATOR.ToLower(),
                Description = "Administration's role. Give access to create and update users with veterinarian roles."
            });

            DbContext.SaveChanges();
        }
    }
}
