﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PetClinic.DbMigration.DataAccess;
using PetClinic.Domain.AggregateModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Infrastructure.Repositories
{
    public abstract class BaseEntityRepository<T, TEtity> : IBaseEntityRepository<T, TEtity>
        where T : class, IEntity<TEtity>
    {
        protected readonly ILogger<BaseEntityRepository<T, TEtity>> Logger;
        protected readonly ApplicationDbContext DbContext;
        public BaseEntityRepository(ApplicationDbContext dbContext, ILogger<BaseEntityRepository<T, TEtity>> logger)
        {
            DbContext = dbContext;
            Logger = logger;
        }

        public async Task Add(T entity, CancellationToken cancellationToken = default)
        {
            try
            {
                await DbContext.AddAsync(entity, cancellationToken);
                await DbContext.SaveChangesAsync(cancellationToken);
            }
            catch(Exception ex)
            {
                Logger.LogError("BaseEntityRepository Add async", ex);
                await DisposeAsync();
                throw;
            }
        }

        public async Task DisposeAsync()
        {
            await DbContext.DisposeAsync();
        }

        public virtual void Remove(T entity)
        {
            try
            {
                DbContext.Remove(entity);
                Complete();
            }
            catch (Exception ex)
            {
                Logger.LogError("BaseEntityRepository Remove", ex);
                DbContext.Dispose();
                throw;
            }
        }

        void IDisposable.Dispose()
        {
            DbContext.Dispose();
        }

        public virtual IEnumerable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return DbContext.Set<T>().Where(predicate);
        }

        public virtual IEnumerable<T> GetAll()
        {
            return DbContext.Set<T>().ToList();
        }

        public virtual async Task<T> GetAsync(TEtity id, CancellationToken cancellationToken = default)
        {
            return await DbContext.Set<T>().FindAsync(new object[] { id }, cancellationToken);
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(CancellationToken cancellationToken = default)
        {
            return await DbContext.Set<T>().ToListAsync(cancellationToken);
        }

        public virtual async Task UpdateAsync(T entity, CancellationToken cancellationToken = default)
        {
            try
            {
                DbContext.Update(entity);
                await DbContext.SaveChangesAsync(cancellationToken);
            }
            catch (Exception ex)
            {
                Logger.LogError($"UpdateAsync {typeof(T)}", ex);
                await DbContext.DisposeAsync();
                throw;
            }
        }

        public virtual void Update(T entity)
        {
            try
            {
                var entry = DbContext.Set<T>().Find(entity.Id);
                DbContext.Entry(entry).CurrentValues.SetValues(entity);
                Complete();
            }
            catch (Exception ex)
            {
                Logger.LogError("BaseEntityRepository Update", ex);
                DbContext.Dispose();
                throw;
            }
        }

        protected int Complete()
        {
            try
            {
                return DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.LogError("BaseEntityRepository Complete", ex);
                return 0;
            }
        }
    }
}
