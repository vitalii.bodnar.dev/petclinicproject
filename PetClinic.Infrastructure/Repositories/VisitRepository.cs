﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PetClinic.DbMigration.DataAccess;
using PetClinic.Domain;
using PetClinic.Domain.AggregateModels;
using PetClinic.Domain.Enums;
using PetClinic.Domain.Models;
using PetClinic.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Infrastructure.Repositories
{
    public class VisitRepository : BaseEntityRepository<Visit, int>, IVisitRepository
    {
        public VisitRepository(ApplicationDbContext context, ILogger<VisitRepository> logger) : base(context, logger) { }

        public override async Task<Visit> GetAsync(int id, CancellationToken cancellationToken = default)
        {
            return await DbContext.Visits
                .Include(x => x.Customer.ClinicUser)
                .Include(x => x.Animal)
                .Include(x => x.Veterinarian.ClinicUser)
                .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }

        public async Task<PagedResult<Visit>> GetVisitsByFilter(VisitFilterModel visitFilterModel)
        {
            var visits = GetFilteredQueryable(DbContext.Visits.AsQueryable(), visitFilterModel);
            return await visits.GetPaged(visitFilterModel.Page, visitFilterModel.PageSize);
        }

        private IQueryable<Visit> GetFilteredQueryable(IQueryable<Visit> query, VisitFilterModel visitFilterModel)
        {
            if (visitFilterModel.CustomerId.HasValue && visitFilterModel.CustomerId != Guid.Empty)
            {
                query = query.Where(x => x.CustomerId == visitFilterModel.CustomerId);
            }

            if (visitFilterModel.VeterinarianId.HasValue && visitFilterModel.VeterinarianId != Guid.Empty)
            {
                query = query.Where(x => x.VeterinarianId == visitFilterModel.VeterinarianId);
            }

            if (visitFilterModel.AnimalId.HasValue && visitFilterModel.AnimalId != 0)
            {
                query = query.Where(x => x.AnimalId == visitFilterModel.AnimalId);
            }

            if (visitFilterModel.AnimalType.HasValue && visitFilterModel.AnimalType != 0)
            {
                query = query.Where(x => x.Animal.Type == (AnimalType)visitFilterModel.AnimalType);
            }

            if (visitFilterModel.IsVisitingHome.HasValue)
            {
                query = query.Where(x => x.IsVisitingHome == visitFilterModel.IsVisitingHome);
            }

            if (visitFilterModel.DateFrom.HasValue)
            {
                query = query.Where(x => x.VisitTime >= visitFilterModel.DateFrom);
            }

            if (visitFilterModel.DateTo.HasValue)
            {
                query = query.Where(x => x.VisitTime <= visitFilterModel.DateTo);
            }

            switch (visitFilterModel.SortOrder)
            {
                case "date_desc":
                    query = query.OrderByDescending(s => s.VisitTime);
                    break;
                default:
                    query = query.OrderBy(s => s.VisitTime);
                    break;
            }

            return query;
        }
    }
}
