﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using PetClinic.DbMigration.DataAccess;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.ApplicationUserModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Infrastructure.Repositories
{
    public class ApplicationUserRepository : BaseEntityRepository<ApplicationUser, Guid>, IApplicationUserRepository
    {
        public ApplicationUserRepository(ApplicationDbContext context, ILogger<ApplicationUserRepository> logger) : base(context, logger) { }

        public async Task<ApplicationUser> FindByEmailAsync(string email, CancellationToken cancellationToken = default)
        {
            var user = await DbContext.ApplicationUsers
                .Include(x => x.ClinicUser)
                .Include(x => x.Roles)
                .FirstOrDefaultAsync(x => x.Email.ToLower() == email.ToLower(), cancellationToken);

            if (user.HasRole(Roles.VETERINARIAN))
            {
                user.ClinicUser.VetInfo = await DbContext.VetInformations.FirstOrDefaultAsync(x => x.ClinicUserId == user.ClinicUser.Id, cancellationToken);
            }

            return user;
        }

        public override async Task<ApplicationUser> GetAsync(Guid id, CancellationToken cancellationToken = default)
        {
            var user = await DbContext.ApplicationUsers
                .Include(x => x.ClinicUser)
                .Include(x => x.Roles)
                .FirstOrDefaultAsync(x => x.Id == id, cancellationToken);

            if (user == null)
            {
                return null;
            }

            if (user.HasRole(Roles.VETERINARIAN))
            {
                user.ClinicUser.VetInfo = await DbContext.VetInformations
                    .Include(x => x.ActiveLicense)
                    .FirstOrDefaultAsync(x => x.ClinicUserId == user.ClinicUser.Id, cancellationToken);
            }

            if (user.HasRole(Roles.CUSTOMER))
            {
                user.ClinicUser.CustomerInfo = await DbContext.CustomerInformations
                    .Include(x => x.Animals)
                    .FirstOrDefaultAsync(x => x.ClinicUserId == user.ClinicUser.Id, cancellationToken);
            }

            return user;
        }

        public async Task<IEnumerable<ApplicationUser>> GetCustomers(CancellationToken cancellationToken = default)
        {
            return await DbContext.ApplicationUsers
                .Include(x => x.Roles)
                .Include(x => x.ClinicUser.CustomerInfo.Animals)
                .Where(x => x.Roles.Any(x => x.Name == Roles.CUSTOMER))
                .ToListAsync(cancellationToken);
        }

        public async Task<IEnumerable<ApplicationUser>> GetVeterinarians(CancellationToken cancellationToken = default)
        {
            return await DbContext.ApplicationUsers
                .Include(x => x.Roles)
                .Include(x => x.ClinicUser.VetInfo.ActiveLicense)
                .Where(x => x.Roles.Any(x => x.Name == Roles.VETERINARIAN))
                .ToListAsync(cancellationToken);
        }

        public bool IsEmailUnique(string email, Guid? id = null)
        {
            if (id.HasValue)
            {
                return !DbContext.ApplicationUsers.Any(x => x.Id != id.Value && x.Email.ToLower() == email.ToLower());
            }

            return !DbContext.ApplicationUsers.Any(x => x.Email.ToLower() == email.ToLower());
        }

        public override async Task UpdateAsync(ApplicationUser entity, CancellationToken cancellationToken = default)
        {
            try
            {
                DbContext.Update(entity.ClinicUser);
                DbContext.Update(entity);
                await DbContext.SaveChangesAsync(cancellationToken);
            }
            catch(Exception ex)
            {
                Logger.LogError("UpdateAsync ApplicationUser ", ex);
            }
        }
    }
}
