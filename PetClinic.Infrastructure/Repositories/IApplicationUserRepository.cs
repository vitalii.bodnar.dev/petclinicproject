﻿using PetClinic.Domain.ApplicationUserModels;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Infrastructure.Repositories
{
    public interface IApplicationUserRepository : IBaseEntityRepository<ApplicationUser, Guid>
    {
        bool IsEmailUnique(string email, Guid? id = null);
        Task<ApplicationUser> FindByEmailAsync(string email, CancellationToken cancellationToken = default);
        Task<IEnumerable<ApplicationUser>> GetCustomers(CancellationToken cancellationToken = default);
        Task<IEnumerable<ApplicationUser>> GetVeterinarians(CancellationToken cancellationToken = default);
    }
}
