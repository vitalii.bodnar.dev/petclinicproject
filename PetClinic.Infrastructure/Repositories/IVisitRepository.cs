﻿using PetClinic.Domain;
using PetClinic.Domain.AggregateModels;
using PetClinic.Domain.Models;
using System.Threading.Tasks;

namespace PetClinic.Infrastructure.Repositories
{
    public interface IVisitRepository : IBaseEntityRepository<Visit, int>
    {
        Task<PagedResult<Visit>> GetVisitsByFilter(VisitFilterModel visitFilterModel);
    }
}
