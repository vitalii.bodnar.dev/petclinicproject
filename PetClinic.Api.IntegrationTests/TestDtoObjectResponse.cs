﻿using PetClinic.Application.Responses;
using System.Collections.Generic;

namespace PetClinic.Api.IntegrationTests
{
    public class TestDtoObjectResponse<T>
    {
        public OperationResultCode Code { get; set; }
        public T ResponseObject { get; set; }
        public IEnumerable<string> ErrorMessages { get; set; }
    }
}
