﻿using AutoMapper;
using FluentAssertions;
using Newtonsoft.Json;
using PetClinic.Application.Dtos.Animals;
using PetClinic.Application.Dtos.Users.Get;
using PetClinic.Application.Dtos.Users.Update;
using PetClinic.Application.Responses;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Tests.Infrastructure;
using PetClinic.Tests.Infrastructure.DatabaseContextArrangeExtensions;
using PetClinic.Tests.Infrastructure.ModelArrangeExtensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using Xunit;

namespace PetClinic.Api.IntegrationTests.Controllers
{
    public class UserControllerTests : IntegrationTestBase<TestStartup>
    {
        [Fact]
        public async void create_user_new_user_should_be_saved()
        {
            // arrange
            var user = UserDtoArrangeExtension.CreateUserDto(password:"Test!@#45");
            var url = GetControllerUrl("create");
            var content = CreateHttpContent(user);
            DatabaseContext.ArrangeApplicationRole(name: Roles.CUSTOMER);
            DatabaseContext.SaveChanges();

            // act
            var response = await Client.PostAsync(url, content, CancellationToken.None);

            // assert
            var contentFromResponse = await response.Content.ReadAsStringAsync();
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            contentFromResponse.Should().NotBeEmpty();

            var objectResponse = GetObjectResponse<GetApplicationUserDto>(contentFromResponse);
            objectResponse.Should().NotBeNull();
            objectResponse.ErrorMessages.Should().HaveCount(0);
            objectResponse.Code.Should().Be(OperationResultCode.Ok);
            objectResponse.ResponseObject.Email.Should().Be(user.Email);
            objectResponse.ResponseObject.ClinicUser.FirstName.Should().Be(user.FirstName);
            objectResponse.ResponseObject.ClinicUser.LastName.Should().Be(user.LastName);
            objectResponse.ResponseObject.Roles.FirstOrDefault(x => x.Name == Roles.CUSTOMER).Should().NotBeNull();
        }

        [Fact]
        public async void create_user_without_names_should_return_error()
        {
            // arrange
            var user = UserDtoArrangeExtension.CreateUserDto(password: "Test!@#45");
            user.FirstName = string.Empty;
            user.LastName = string.Empty;
            var url = GetControllerUrl("create");
            var content = CreateHttpContent(user);
            DatabaseContext.ArrangeApplicationRole(name: Roles.CUSTOMER);
            DatabaseContext.SaveChanges();

            // act
            var response = await Client.PostAsync(url, content, CancellationToken.None);

            // assert
            var contentFromResponse = await response.Content.ReadAsStringAsync();
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            contentFromResponse.Should().NotBeEmpty();
            var objectResponse = GetObjectResponse<GetApplicationUserDto>(contentFromResponse);
            objectResponse.Code.Should().Be(OperationResultCode.Error);
            objectResponse.ErrorMessages.Should().HaveCount(2);
        }

        [Fact]
        public async void create_user_with_uncorrect_password_should_return_error()
        {
            // arrange
            var user = UserDtoArrangeExtension.CreateUserDto(password: "pass");
            var url = GetControllerUrl("create");
            var content = CreateHttpContent(user);
            DatabaseContext.ArrangeApplicationRole(name: Roles.CUSTOMER);
            DatabaseContext.SaveChanges();

            // act
            var response = await Client.PostAsync(url, content, CancellationToken.None);

            // assert
            var contentFromResponse = await response.Content.ReadAsStringAsync();
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            contentFromResponse.Should().NotBeEmpty();
            var objectResponse = GetObjectResponse<GetApplicationUserDto>(contentFromResponse);
            objectResponse.Code.Should().Be(OperationResultCode.Error);
            objectResponse.ErrorMessages.Should().HaveCount(2);
        }

        [Fact]
        public async void get_user_by_id_should_return_correct_user()
        {
            // arrange
            var applicationUser = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            DatabaseContext.SaveChanges();
            var route = GetUserUrl(applicationUser.Id);

            // act
            var response = await Client.GetAsync(route, CancellationToken.None);

            // assert
            var contentFromResponse = await response.Content.ReadAsStringAsync();
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            contentFromResponse.Should().NotBeEmpty();

            var objectResponse = GetObjectResponse<GetApplicationUserDto>(contentFromResponse);
            objectResponse.Should().NotBeNull();
            objectResponse.ErrorMessages.Should().HaveCount(0);
            objectResponse.Code.Should().Be(OperationResultCode.Ok);
            objectResponse.ResponseObject.Email.Should().Be(applicationUser.Email);
            objectResponse.ResponseObject.Id.Should().Be(applicationUser.Id);
            objectResponse.ResponseObject.ClinicUser.FirstName.Should().Be(applicationUser.ClinicUser.FirstName);
            objectResponse.ResponseObject.ClinicUser.LastName.Should().Be(applicationUser.ClinicUser.LastName);
            objectResponse.ResponseObject.Roles.FirstOrDefault(x => x.Name == Roles.CUSTOMER).Should().NotBeNull();
        }

        [Fact]
        public async void get_customers_should_return_correct_users()
        {
            // arrange
            var user1 = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var user2 = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            var user3 = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            DatabaseContext.SaveChanges();
            var route = GetControllerUrl("customers");

            // act
            var response = await Client.GetAsync(route, CancellationToken.None);

            // assert
            var contentFromResponse = await response.Content.ReadAsStringAsync();
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            contentFromResponse.Should().NotBeEmpty();

            var objectResponse = GetObjectResponse<List<GetApplicationUserDto>>(contentFromResponse);
            objectResponse.Should().NotBeNull();
            objectResponse.ErrorMessages.Should().HaveCount(0);
            objectResponse.Code.Should().Be(OperationResultCode.Ok);

            var responseUser1 = objectResponse.ResponseObject.FirstOrDefault(x => x.Id == user1.Id);
            var responseUser2 = objectResponse.ResponseObject.FirstOrDefault(x => x.Id == user2.Id);
            var responseUser3 = objectResponse.ResponseObject.FirstOrDefault(x => x.Id == user3.Id);

            responseUser1.Should().NotBeNull();
            responseUser1.Email.Should().Be(user1.Email);
            responseUser1.ClinicUser.FirstName.Should().Be(user1.ClinicUser.FirstName);
            responseUser1.ClinicUser.LastName.Should().Be(user1.ClinicUser.LastName);
            responseUser1.Roles.FirstOrDefault(x => x.Name == Roles.CUSTOMER).Should().NotBeNull();

            responseUser2.Should().NotBeNull();
            responseUser2.Email.Should().Be(user2.Email);
            responseUser2.ClinicUser.FirstName.Should().Be(user2.ClinicUser.FirstName);
            responseUser2.ClinicUser.LastName.Should().Be(user2.ClinicUser.LastName);
            responseUser2.Roles.FirstOrDefault(x => x.Name == Roles.CUSTOMER).Should().NotBeNull();

            responseUser3.Should().NotBeNull();
            responseUser3.Email.Should().Be(user3.Email);
            responseUser3.ClinicUser.FirstName.Should().Be(user3.ClinicUser.FirstName);
            responseUser3.ClinicUser.LastName.Should().Be(user3.ClinicUser.LastName);
            responseUser3.Roles.FirstOrDefault(x => x.Name == Roles.CUSTOMER).Should().NotBeNull();
        }

        [Fact]
        public async void update_should_save_user_data()
        {
            // arrange
            var user = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            DatabaseContext.SaveChanges();
            var url = GetControllerUrl("update");
            UpdateUserDto updateUser = GetService<IMapper>().Map<UpdateUserDto>(user);
            updateUser.PhoneNumber = "880-235-3535";
            updateUser.ClinicUser.FirstName = "UpdatedName";
            var newAnimalName = "UpdatedAnimalName";
            var animal = updateUser.ClinicUser.CustomerInfo.Animals.FirstOrDefault();
            animal.Name = newAnimalName;
            updateUser.ClinicUser.CustomerInfo.Animals = new List<UpdateAnimalDto>() { animal };
            var content = CreateHttpContent(updateUser);

            // act
            var response = await Client.PutAsync(url, content, CancellationToken.None);

            // assert
            var contentFromResponse = await response.Content.ReadAsStringAsync();
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            contentFromResponse.Should().NotBeEmpty();

            var objectResponse = GetObjectResponse<UpdateUserDto>(contentFromResponse);
            objectResponse.Should().NotBeNull();
            objectResponse.ErrorMessages.Should().HaveCount(0);
            objectResponse.Code.Should().Be(OperationResultCode.Ok);

            objectResponse.ResponseObject.PhoneNumber.Should().Be(updateUser.PhoneNumber);
            objectResponse.ResponseObject.Email.Should().Be(user.Email);
            objectResponse.ResponseObject.ClinicUser.FirstName.Should().Be(updateUser.ClinicUser.FirstName);
            objectResponse.ResponseObject.ClinicUser.LastName.Should().Be(user.ClinicUser.LastName);
            objectResponse.ResponseObject.ClinicUser.CustomerInfo.Animals.Should().HaveCount(1);
            objectResponse.ResponseObject.ClinicUser.CustomerInfo.Animals.FirstOrDefault().Name.Should().Be(newAnimalName);
        }

        [Fact]
        public async void update_without_required_properties_should_save_user_data()
        {
            // arrange
            var user = DatabaseContext.ArrangeApplicationUser(addRoles: true);
            DatabaseContext.SaveChanges();
            var url = GetControllerUrl("update");
            UpdateUserDto updateUser = GetService<IMapper>().Map<UpdateUserDto>(user);
            updateUser.PhoneNumber = string.Empty;
            updateUser.ClinicUser.FirstName = string.Empty;
            var content = CreateHttpContent(updateUser);

            // act
            var response = await Client.PutAsync(url, content, CancellationToken.None);

            // assert
            var contentFromResponse = await response.Content.ReadAsStringAsync();
            response.Should().NotBeNull();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
            contentFromResponse.Should().NotBeEmpty();

            var objectResponse = GetObjectResponse<UpdateUserDto>(contentFromResponse);
            objectResponse.Should().NotBeNull();
            objectResponse.ErrorMessages.Should().HaveCount(2);
            objectResponse.Code.Should().Be(OperationResultCode.Error);
        }

        private static string GetControllerUrl(string route = null)
        {
            if (string.IsNullOrEmpty(route))
            {
                return "api/users/";
            }

            return $"api/users/{route}";
        }

        public static string GetUserUrl(Guid id)
        {
            return $"api/users/{id}";
        }

        private HttpContent CreateHttpContent(object request)
        {
            var myContent = JsonConvert.SerializeObject(request);
            var buffer = Encoding.UTF8.GetBytes(myContent);
            var byteContent = new ByteArrayContent(buffer);
            byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return byteContent;
        }

        private TestDtoObjectResponse<T> GetObjectResponse<T>(string responsJson)
        {
            return JsonConvert.DeserializeObject<TestDtoObjectResponse<T>>(responsJson);
        }
    }
}
