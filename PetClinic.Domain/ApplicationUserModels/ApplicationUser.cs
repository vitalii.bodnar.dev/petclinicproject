﻿using Microsoft.AspNetCore.Identity;
using PetClinic.Domain.AggregateModels;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PetClinic.Domain.ApplicationUserModels
{
    public class ApplicationUser : IdentityUser<Guid>, IEntity<Guid>
    {
        public string PasswordSalt { get; set; }
        public bool IsLockedOut => LockoutEnabled && LockoutEnd >= DateTimeOffset.UtcNow;
        public IList<ApplicationRole> Roles { get; private set; } = new List<ApplicationRole>();
        public ClinicUser ClinicUser { get; set; } = new ClinicUser();

        public void AddRolesToUser(IEnumerable<ApplicationRole> roles)
        {
            foreach (var role in roles)
            {
                Roles.Add(role);
            }
        }

        public void AddRoleToUser(ApplicationRole role)
        {
            if(role?.Id == null)
            {
                return;
            }

            Roles.Add(role);
        }

        public bool HasRole(string roleName)
        {
            return Roles.Any(x => x.NormalizedName == roleName.ToLower());
        }
    }
}
