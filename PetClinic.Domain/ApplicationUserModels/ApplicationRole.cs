﻿using Microsoft.AspNetCore.Identity;
using PetClinic.Domain.AggregateModels;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace PetClinic.Domain.ApplicationUserModels
{
    public class ApplicationRole : IdentityRole<Guid>, IEntity<Guid>
    {
        public ApplicationRole() : base()
        {
        }

        public ApplicationRole(string roleName) : base(roleName)
        {
        }

        public ApplicationRole(string roleName, string description) : base(roleName)
        {
            Description = description;
        }
        public string Description { get; set; }
        [JsonIgnore]
        [IgnoreDataMember]
        public virtual IEnumerable<ApplicationUser> Users { get; set; } = new List<ApplicationUser>();
    }
}
