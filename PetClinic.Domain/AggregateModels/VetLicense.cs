﻿using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace PetClinic.Domain.AggregateModels
{
    public class VetLicense : IEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int VetInfoId { get; set; }

        [Required]
        public VetInfo VetInfo { get; set; }

        [Required]
        [MaxLength(50)]
        public string Number { get; set; }

        [Required]
        [Column(TypeName="date")]
        public DateTime DateOfIssue { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateOfExpiry { get; set; }

        [Required]
        [MaxLength(200)]
        public string IssuingAuthorityName { get; set; }

        [Required]
        [MaxLength(50)]
        public string IssuingAuthorityId { get; set; }
    }
}
