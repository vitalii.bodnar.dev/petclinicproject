﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PetClinic.Domain.AggregateModels
{
    public interface IEntity : IEntity<int>
    {

    }

    public interface IEntity<T>
    {
        public T Id { get; set; }
    }
}
