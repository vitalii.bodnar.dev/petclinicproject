﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PetClinic.Domain.AggregateModels.Animals
{
    public class Vaccination : IEntity
    {
        [Required]
        public int Id { get; set; }
        public Animal Animal { get; set; }
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }
        [MaxLength(50)]
        public string Code { get; set; }
        [MaxLength(250)]
        public string Description { get; set; }
    }
}
