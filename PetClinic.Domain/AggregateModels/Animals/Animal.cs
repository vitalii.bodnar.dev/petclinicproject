﻿using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PetClinic.Domain.AggregateModels.Animals
{
    public class Animal : IEntity
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public int OwnerId { get; set; }

        public CustomerInfo Owner { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [MaxLength(3000)]
        public string Description { get; set; }

        [Required]
        [Column(TypeName = "date")]
        public DateTime DateOfBirth { get; set; }

        public AnimalType Type { get; set; }

        public IList<Vaccination> Vaccinations { get; set; } = new List<Vaccination>();
    }
}
