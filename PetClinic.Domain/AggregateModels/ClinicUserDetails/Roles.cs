﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetClinic.Domain.AggregateModels.ClinicUserDetails
{
    public static class Roles
    {
        public const string VETERINARIAN = "Veterinarian";
        public const string ADMINISTRATOR = "Administrator";
        public const string CUSTOMER = "Customer";
    }
}
