﻿using PetClinic.Domain.AggregateModels.Animals;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PetClinic.Domain.AggregateModels.ClinicUserDetails
{
    public class CustomerInfo : IEntity
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public Guid ClinicUserId { get; set; }
        public ClinicUser ClinicUser { get; set; }
        public IEnumerable<Animal> Animals { get; set; } = new List<Animal>();
    }
}
