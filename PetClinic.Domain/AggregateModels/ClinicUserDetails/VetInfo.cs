﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PetClinic.Domain.AggregateModels.ClinicUserDetails
{
    public class VetInfo : IEntity
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public Guid ClinicUserId { get; set; }
        public ClinicUser ClinicUser { get; set; }
        [MaxLength(3000)]
        public string Description { get; set; }
        public VetLicense ActiveLicense { get; set; }

    }
}
