﻿using PetClinic.Domain.AggregateModels.Animals;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.ApplicationUserModels;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace PetClinic.Domain.AggregateModels
{
    public class Visit : IEntity
    {
        //ctor for ef
        public Visit() { }

        public Visit(ApplicationUser veterinarian, ApplicationUser customer, Animal animal)
        {
            SetVeterinarian(veterinarian);
            SetCustomerWithAnimal(customer, animal);
        }

        public int Id { get; set; }
        
        public ApplicationUser Veterinarian { get; private set; }

        [Required]
        public Guid VeterinarianId { get; private set; }

        public ApplicationUser Customer { get; private set; }

        [Required]
        public Guid CustomerId { get; private set; }

        public Animal Animal { get; private set; }

        [Required]
        public int AnimalId { get; private set; }

        [Required]
        public DateTime VisitTime { get; set; }

        public bool IsVisitingHome { get; set; }

        public string VeterinarianComment { get; set; }

        public string CustomerComment { get; set; }

        public void SetVeterinarian(ApplicationUser veterinarian)
        {
            if (veterinarian == null)
            {
                throw new ArgumentException("Cannot create visit without: ", nameof(veterinarian));
            }

            if (!veterinarian.Roles.Any(x => x.Name == Roles.VETERINARIAN))
            {
                throw new ArgumentException("Not able to create a visit. Chosen vet is not found!");
            }

            Veterinarian = veterinarian;
        }

        public void SetCustomerWithAnimal(ApplicationUser customer, Animal animal)
        {
            if (customer == null)
            {
                throw new ArgumentException("Cannot create visit without: ", nameof(customer));
            }

            if (animal == null)
            {
                throw new ArgumentException("Cannot create visit without: ", nameof(animal));
            }

            if (animal.Owner.ClinicUser.Id != customer.ClinicUser.Id)
            {
                throw new ArgumentException("Not able to create a visit. Customer ID does not match Animal Owner ID");
            }

            Customer = customer;
            Animal = animal;
        }
    }
}
