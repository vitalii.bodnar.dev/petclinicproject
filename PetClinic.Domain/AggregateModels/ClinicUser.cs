﻿using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.ApplicationUserModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PetClinic.Domain.AggregateModels
{
    public class ClinicUser : IEntity<Guid>
    {
        [Required]
        public Guid Id { get; set; }
        [Required]
        public Guid ApplicationUserId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string SecondName { get; set; }
        [Required]
        [MaxLength(50)]
        public string LastName { get; set; }
        public CustomerInfo CustomerInfo { get; set; }
        public VetInfo VetInfo { get; set; }
    }
}
