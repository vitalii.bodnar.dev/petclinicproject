﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetClinic.Domain.Enums
{
    public enum AnimalType
    {
        Mammal = 1,
        Bird = 2,
        Reptile = 3,
        Amphibian = 4,
        Fish = 5
    }
}
