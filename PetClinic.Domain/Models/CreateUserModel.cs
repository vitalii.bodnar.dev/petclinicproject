﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetClinic.Domain.Models
{
    public class CreateUserModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
