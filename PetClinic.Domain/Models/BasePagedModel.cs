﻿namespace PetClinic.Domain.Models
{
    public class BasePagedModel
    {
        public string SortOrder { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
