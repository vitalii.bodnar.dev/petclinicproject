﻿using System;

namespace PetClinic.Domain.Models
{
    public class UpdateVisitModel
    {
        public int Id { get; set; }
        public Guid VeterinarianId { get; set; }
        public Guid CustomerId { get; set; }
        public int AnimalId { get; set; }
        public bool IsVisitingHome { get; set; }
        public DateTime VisitTime { get; set; }
        public string VeterinarianComment { get; set; }
        public string CustomerComment { get; set; }
    }
}
