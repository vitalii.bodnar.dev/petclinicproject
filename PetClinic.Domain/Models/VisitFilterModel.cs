﻿using System;

namespace PetClinic.Domain.Models
{
    public class VisitFilterModel : BasePagedModel
    {
        public Guid? VeterinarianId { get; set; }
        public Guid? CustomerId { get; set; }
        public int? AnimalId { get; set; }
        public int? AnimalType { get; set; }
        public bool? IsVisitingHome { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
