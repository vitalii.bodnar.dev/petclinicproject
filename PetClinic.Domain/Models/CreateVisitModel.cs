﻿using System;

namespace PetClinic.Domain.Models
{
    public class CreateVisitModel
    {
        public Guid VeterinarianId { get; set; }
        public Guid CustomerId { get; set; }
        public int AnimalId { get; set; }
        public DateTime VisitTime { get; set; }
        public bool IsVisitingHome { get; set; }
    }
}
