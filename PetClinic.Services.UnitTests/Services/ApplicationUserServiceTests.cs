﻿using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using NSubstitute;
using PetClinic.Domain.ApplicationUserModels;
using PetClinic.Infrastructure.Repositories;
using PetClinic.Service.Helpers;
using PetClinic.Service.Services;
using Xunit;

namespace PetClinic.Services.UnitTests.Services
{
    public class ApplicationUserServiceTests
    {
        private readonly IApplicationUserService _sut;

        public ApplicationUserServiceTests()
        {
            var logger = Mock.Of<ILogger<ApplicationUserService>>();
            _sut = new ApplicationUserService(
                    Substitute.For<IApplicationUserRepository>(),
                    Substitute.For<IApplicationRoleRepository>(),
                    logger
                );
        }

        [Fact]
        public void check_password_should_return_true_for_correct_password()
        {
            // arrange
            var user = new ApplicationUser();
            string password = "P@ssw0rd";
            user.GenerateHashPassword(password);

            // act
            var result = _sut.CheckPassword(user, password);

            // assert
            result.Should().BeTrue();
        }

        [Fact]
        public void check_password_should_return_false_for_incorrect_password()
        {
            // arrange
            var user = new ApplicationUser();
            string password = "P@ssw0rd";
            user.GenerateHashPassword(password);

            // act
            var result = _sut.CheckPassword(user, password.Remove(1, 1));

            // assert
            result.Should().BeFalse();
        }
    }
}
