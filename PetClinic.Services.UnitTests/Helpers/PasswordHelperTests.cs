﻿using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PetClinic.Domain.ApplicationUserModels;
using PetClinic.Service.Helpers;
using System;
using Xunit;
using TestToolsAssert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace PetClinic.Services.UnitTests.Helpers
{
    public class PasswordHelperTests
    {
        [Fact]
        public void generate_hash_password_should_generate_password_hash_and_password_salt()
        {
            // arrange
            var user = new ApplicationUser();
            string password = "P@ssw0rd";

            // act
            user.GenerateHashPassword(password);

            // assert
            user.PasswordHash.Should().NotBeEmpty();
            user.PasswordSalt.Should().NotBeEmpty();
        }

        [Fact]
        [ExpectedException(typeof(ArgumentNullException))]
        public void generate_hash_password_with_empty_password_should_throw_exception()
        {
            // arrange
            var user = new ApplicationUser();

            // act
            try
            {
                user.GenerateHashPassword(string.Empty);
            }

            // assert
            catch(ArgumentNullException ex)
            {
                ex.Message.Should().Contain("Password is null or empty.");
            }
            catch(Exception ex)
            {
                TestToolsAssert.Fail(string.Format("Unexpected exception of type {0} caught: {1}", ex.GetType(), ex.Message));
            }
        }

        [Fact]
        public void generate_hash_password_should_generate_different_hash_and_salt_for_different_users()
        {
            // arrange
            var user1 = new ApplicationUser();
            var user2 = new ApplicationUser();
            string password = "P@ssw0rd";

            // act
            user1.GenerateHashPassword(password);
            user2.GenerateHashPassword(password);

            // assert
            user1.PasswordHash.Should().NotBeEmpty();
            user1.PasswordSalt.Should().NotBeEmpty();

            user2.PasswordHash.Should().NotBeEmpty();
            user2.PasswordSalt.Should().NotBeEmpty();

            user1.PasswordHash.Should().NotBe(user2.PasswordHash);
            user1.PasswordSalt.Should().NotBe(user2.PasswordSalt);
        }

        [Fact]
        public void verify_password_should_return_true_for_correct_password()
        {
            // arrange
            var user = new ApplicationUser();
            string password = "P@ssw0rd";
            user.GenerateHashPassword(password);

            // act
            var result = user.VerifyPassword(password);

            // assert
            result.Should().BeTrue();
        }

        [Fact]
        public void verify_password_should_return_false_for_empty_password()
        {
            // arrange
            var user = new ApplicationUser();
            string password = "P@ssw0rd";
            user.GenerateHashPassword(password);

            // act
            var result = user.VerifyPassword(string.Empty);

            // assert
            result.Should().BeFalse();
        }

        [Fact]
        public void verify_password_should_return_false_for_incorrect_password()
        {
            // arrange
            var user = new ApplicationUser();
            string password = "P@ssw0rd";
            user.GenerateHashPassword(password);

            // act
            var result = user.VerifyPassword(password.Remove(1,1));

            // assert
            result.Should().BeFalse();
        }
    }
}
