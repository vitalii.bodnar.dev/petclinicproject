## General info
The project is a rest-api dedicated to a vet clinic operation. The main possibilities are registration of a pet as well as user and scheduling the appointment with a doctor. The main goal of creating this project was to test and to improve my knowledge in certain aspects.
	
## Technologies
Project is created with:
* .Net version: 5.0
* Autofac version: 7.1.0
* AutoMapper version: 10.1.1
* FluentValidation version: 9.4.0
* MediatR version: 9.0.0
* JwtBearer version: 3.1.11
* EntityFrameworkCore version: 5.0.2
* Serilog version: 2.10.0
* Swagger version: 5.6.3
* OpenIddict version: 3.0.0

Tests are created with:
* .NET.Test.Sdk version: 16.9.4
* Bogus version: 33.0.2
* XUnit version: 2.4.1
* Moq version: 4.16.1
* FluentAssertions version: 5.10.3
* NSubstitute version: 4.2.2
