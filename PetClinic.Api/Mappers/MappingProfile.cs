﻿using AutoMapper;
using PetClinic.Application.Dtos;
using PetClinic.Application.Dtos.Animals;
using PetClinic.Application.Dtos.Users.Create;
using PetClinic.Application.Dtos.Users.Get;
using PetClinic.Application.Dtos.Users.Update;
using PetClinic.Application.Dtos.Visits.Create;
using PetClinic.Application.Dtos.Visits.Get;
using PetClinic.Application.Dtos.Visits.Update;
using PetClinic.Application.Filters;
using PetClinic.Domain;
using PetClinic.Domain.AggregateModels;
using PetClinic.Domain.AggregateModels.Animals;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.ApplicationUserModels;
using PetClinic.Domain.Models;

namespace PetClinic.Api.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<ApplicationRoleDto, ApplicationRole>();
            CreateMap<ApplicationRole, ApplicationRoleDto>();

            CreateMap<GetClinicUserDto, ClinicUser>();
            CreateMap<ClinicUser, GetClinicUserDto>();

            CreateMap<UpdateClinicUserDto, ClinicUser>();
            CreateMap<ClinicUser, UpdateClinicUserDto>();

            CreateMap<UpdateUserDto, ApplicationUser>();
            CreateMap<ApplicationUser, UpdateUserDto>();

            CreateMap<GetCustomerInfoDto, CustomerInfo>();
            CreateMap<CustomerInfo, GetCustomerInfoDto>();

            CreateMap<UpdateCustomerInfoDto, CustomerInfo>();
            CreateMap<CustomerInfo, UpdateCustomerInfoDto>();

            CreateMap<GetApplicationUserDto, ApplicationUser>();
            CreateMap<ApplicationUser, GetApplicationUserDto>();

            //

            CreateMap<GetVeterinarianDto, ApplicationUser>();
            CreateMap<ApplicationUser, GetVeterinarianDto>();

            CreateMap<GetVetClinicUserDto, ClinicUser>();
            CreateMap<ClinicUser, GetVetClinicUserDto>();

            CreateMap<GetVetInfoDto, VetInfo>();
            CreateMap<VetInfo, GetVetInfoDto>();

            CreateMap<GetVetLicenseDto, VetLicense>();
            CreateMap<VetLicense, GetVetLicenseDto>();

            //

            CreateMap<UpdateVeterinarianDto, ApplicationUser>();
            CreateMap<ApplicationUser, UpdateVeterinarianDto>();

            CreateMap<UpdateVetClinicUserDto, ClinicUser>();
            CreateMap<ClinicUser, UpdateVetClinicUserDto>();

            CreateMap<UpdateVetInfoDto, VetInfo>();
            CreateMap<VetInfo, UpdateVetInfoDto>();

            CreateMap<UpdateVetLicenseDto, VetLicense>();
            CreateMap<VetLicense, UpdateVetLicenseDto>();

            //visits

            CreateMap<GetVisitDto, Visit>();
            CreateMap<Visit, GetVisitDto>();

            //models

            CreateMap<CreateVisitDto, CreateVisitModel>();
            CreateMap<UpdateVisitDto, UpdateVisitModel>();
            CreateMap<VisitFilter, VisitFilterModel>();
            CreateMap<VisitFilterModel, VisitFilter>();
            CreateMap<CreateUserModel, CreateApplicationUserDto>();
            CreateMap<CreateApplicationUserDto, CreateUserModel>();
            CreateMap<CreateUserModel, CreateVeterinarianDto>();
            CreateMap<CreateVeterinarianDto, CreateUserModel>();

            CreateMap(typeof(PagedResult<>), typeof(PagedResultDto<>));
            CreateMap(typeof(PagedResultDto<>), typeof(PagedResult<>));
            //

            CreateMap<UpdateAnimalDto, Animal>();
            CreateMap<Animal, UpdateAnimalDto>();

            CreateMap<GetAnimalDto, Animal>();
            CreateMap<Animal, GetAnimalDto>();
        }
    }
}
