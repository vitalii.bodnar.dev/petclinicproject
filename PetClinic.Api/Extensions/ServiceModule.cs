﻿using Autofac;
using PetClinic.Infrastructure.Repositories;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace PetClinic.Api.Extensions
{
    public class ServiceModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);
            builder.RegisterAssemblyTypes(typeof(IBaseEntityRepository<,>).GetTypeInfo().Assembly).AsImplementedInterfaces();
            builder.RegisterAssemblyTypes(typeof(IBaseEntityService<,>).GetTypeInfo().Assembly).AsImplementedInterfaces();
        }
    }
}
