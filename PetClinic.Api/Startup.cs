using Autofac;
using Autofac.Integration.WebApi;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using PetClinic.Api.Controllers;
using PetClinic.Api.Extensions;
using Swashbuckle.AspNetCore.SwaggerUI;
using System.Reflection;

namespace PetClinic.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureMvc();
            ConfigureDataAccessLayer(services, Configuration);
            services.ConfigureOpenIddict();
            ConfigureAuthentication(services, Configuration);
            services.ConfigureSwagger();
            services.ConfigureMapper();
            services.ConfigureMediatR();
            services.ConfigureFluentValidation();
        }

        protected virtual void ConfigureAuthentication(IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureAuthentication(configuration);
        }

        protected virtual void ConfigureDataAccessLayer(IServiceCollection services, IConfiguration configuration)
        {
            services.ConfigureDbContextes(configuration);
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder.RegisterApiControllers(Assembly.GetAssembly(typeof(UsersController)));
            builder.RegisterModule<ServiceModule>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseSwagger();
            app.UseSwaggerUI(ui =>
            {
                ui.DocExpansion(DocExpansion.List);
                ui.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
            });
        }
    }
}
