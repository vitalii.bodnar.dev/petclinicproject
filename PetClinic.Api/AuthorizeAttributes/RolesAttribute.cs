﻿using Microsoft.AspNetCore.Authorization;
using System;

namespace PetClinic.Api.AuthorizeAttributes
{
    public class RolesAttribute : AuthorizeAttribute
    {
        public RolesAttribute(params string[] roles)
        {
            Roles = String.Join(",", roles);
        }
    }
}
