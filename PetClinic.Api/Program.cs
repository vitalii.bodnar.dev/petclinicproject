using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.RollingFile.Extension;
using System;
using System.IO;

namespace PetClinic.Api
{
    public class Program
    {
        public static readonly string Namespace = typeof(Program).Namespace;
        public static readonly string AppName = Namespace.Substring(Namespace.LastIndexOf('.', Namespace.LastIndexOf('.') - 1) + 1);

        public static IConfiguration Configuration { get; } = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
            .Build();

        public static int Main(string[] args)
        {
            Log.Logger = CreateLogger(Configuration);

            try
            {
                Log.Information("Configuring web host ({ApplicationContext})...", AppName);
                var host = BuildWebHost(Configuration, args);
                Log.Information("Starting web host ({ApplicationContext})...", AppName);
                host.Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Program terminated unexpectedly ({ApplicationContext})!", AppName);
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        private static ILogger CreateLogger(IConfiguration configuration)
        {
            string logTemplate = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u}] [{SourceContext}] {Message}{NewLine}{Exception}";
            return new LoggerConfiguration()
                          .MinimumLevel.Verbose()
                          .Enrich.WithProperty("ApplicationContext", "")
                          .Enrich.FromLogContext()
                          .WriteTo.SizeRollingFile("Logs/clinic-api-log-{Date}.log", outputTemplate: logTemplate)
                          .ReadFrom.Configuration(configuration)
                          .CreateLogger();
        }

        public static IWebHost BuildWebHost(IConfiguration configuration, string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .ConfigureAppConfiguration(configurationBuilder => configurationBuilder.AddConfiguration(configuration))
                .ConfigureServices(services => services.AddAutofac())
                .UseSerilog()
                .Build();
    }
}
