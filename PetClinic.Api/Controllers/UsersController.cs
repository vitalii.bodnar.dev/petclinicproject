﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PetClinic.Api.AuthorizeAttributes;
using PetClinic.Application.Commands;
using PetClinic.Application.Queries;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using Serilog;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [SwaggerTag("Users controller")]
    public class UsersController : Controller
    {
        private readonly IMediator _mediator;

        public UsersController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        [Route("Create")]
        [SwaggerOperation(Summary = "Create user", Description = "Create user")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "User has created")]
        [SwaggerResponse(401, Type = typeof(FileContentResult), Description = "Unauthorized!")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "User has not created")]
        public async Task<IActionResult> Create(CreateUserCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return CreatedAtAction("Create", new { routeValues = result.ResponseObject }, result);
            }
            catch (Exception ex)
            {
                Log.Fatal("UserController Create exception: ", ex);
                return BadRequest(ex);
            }
        }

        [Authorize]
        [HttpPut]
        [Route("Update")]
        [SwaggerOperation(Summary = "Update user", Description = "Update user")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "User has updated")]
        [SwaggerResponse(401, Type = typeof(FileContentResult), Description = "Unauthorized!")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "User has not updated")]
        public async Task<IActionResult> Update(UpdateUserCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return CreatedAtAction("Update", new { routeValues = result.ResponseObject }, result);
            }
            catch (Exception ex)
            {
                Log.Fatal("UserController Update exception: ", ex);
                return BadRequest(ex);
            }
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Get user by id", Description = "Returns user by Id")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "User")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "User with that id does not exist")]
        public async Task<IActionResult> GetUserById(Guid id, CancellationToken cancellationToken)
        {
            try
            {
                var query = new GetUserByIdQuery(id);
                var result = await _mediator.Send(query, cancellationToken);
                return result != null ? (IActionResult)Ok(result) : NotFound();
            }
            catch (Exception ex)
            {
                Log.Fatal("UserController GetUserById error", ex);
                return BadRequest(ex);
            }
        }

        [Roles(Roles.VETERINARIAN, Roles.ADMINISTRATOR)]
        [HttpGet]
        [Route("Customers")]
        [SwaggerOperation(Summary = "Returns list of customers", Description = "Returns list of customers")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "List of customers")]
        [SwaggerResponse(401, Type = typeof(FileContentResult), Description = "Unauthorized!")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "Customers does not exist")]
        public async Task<IActionResult> GetAll(CancellationToken cancellationToken)
        {
            try
            {
                var query = new GetCustomersQuery();
                var result = await _mediator.Send(query, cancellationToken);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Log.Fatal("UserController GetAllCustomers exception: ", ex);
                return BadRequest(ex);
            }
        }
    }
}
