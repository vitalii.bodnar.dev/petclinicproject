﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PetClinic.Api.AuthorizeAttributes;
using PetClinic.Application.Commands;
using PetClinic.Application.Filters;
using PetClinic.Application.Queries;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using Serilog;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [SwaggerTag("Visits controller")]
    public class VisitsController : Controller
    {
        private readonly IMediator _mediator;

        public VisitsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [Authorize]
        [HttpPost]
        [Route("Create")]
        [SwaggerOperation(Summary = "Create visit", Description = "Create visit")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "Visit has created")]
        [SwaggerResponse(401, Type = typeof(FileContentResult), Description = "Unauthorized!")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "Visit has not created")]
        public async Task<IActionResult> Create(CreateVisitCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return CreatedAtAction("Create", new { routeValues = result.ResponseObject }, result);
            }
            catch (Exception ex)
            {
                Log.Fatal("VisitController Create exception: ", ex);
                return BadRequest(ex);
            }
        }

        [Authorize]
        [HttpPut]
        [Route("Update")]
        [SwaggerOperation(Summary = "Update visit", Description = "Update visit")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "Visit has updated")]
        [SwaggerResponse(401, Type = typeof(FileContentResult), Description = "Unauthorized!")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "Visit has not updated")]
        public async Task<IActionResult> Update(UpdateVisitCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return CreatedAtAction("Update", new { routeValues = result.ResponseObject }, result);
            }
            catch (Exception ex)
            {
                Log.Fatal("VisitsController Update exception: ", ex);
                return BadRequest(ex);
            }
        }

        [Authorize]
        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Get visit by id", Description = "Returns visit by Id")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "Visit")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "Visit with that id does not exist")]
        public async Task<IActionResult> GetVisitById(int id, CancellationToken cancellationToken)
        {
            try
            {
                var query = new GetVisitByIdQuery(id);
                var result = await _mediator.Send(query, cancellationToken);
                return result != null ? (IActionResult)Ok(result) : NotFound();
            }
            catch (Exception ex)
            {
                Log.Fatal("VisitController GetVisitById error", ex);
                return BadRequest(ex);
            }
        }

        [Roles(Roles.ADMINISTRATOR, Roles.VETERINARIAN)]
        [HttpPost]
        [Route("Filter")]
        [SwaggerOperation(Summary = "Get filtered visits", Description = "Returns filtered visits")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "Visits")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "Filtered visits does not exist")]
        public async Task<IActionResult> GetFilteredVisits(VisitFilter filter, CancellationToken cancellationToken)
        {
            try
            {
                var query = new GetVisitsQuery(filter);
                var result = await _mediator.Send(query, cancellationToken);
                return result != null ? (IActionResult)Ok(result) : NotFound();
            }
            catch (Exception ex)
            {
                Log.Fatal("VisitController GetVisits error", ex);
                return BadRequest(ex);
            }
        }
    }
}
