﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PetClinic.Api.AuthorizeAttributes;
using PetClinic.Application.Commands;
using PetClinic.Application.Queries;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using Serilog;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [SwaggerTag("Veterinarian controller")]
    public class VeterinariansController : ControllerBase
    {
        private readonly IMediator _mediator;
        
        public VeterinariansController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [Roles(Roles.ADMINISTRATOR)]
        [HttpPost]
        [Route("Create")]
        [SwaggerOperation(Summary = "Create veterinarian", Description = "Create veterinarian")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "Veterinarian has created")]
        [SwaggerResponse(401, Type = typeof(FileContentResult), Description = "Unauthorized!")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "Veterinarian has not created")]
        public async Task<IActionResult> Create(CreateVeterinarianCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return CreatedAtAction("Create", new { routeValues = result.ResponseObject }, result);
            }
            catch (Exception ex)
            {
                Log.Fatal("VeterinarianController Create exception: ", ex);
                return BadRequest(ex);
            }
        }

        [Authorize]
        [HttpPut]
        [Route("Update")]
        [SwaggerOperation(Summary = "Update veterinarian", Description = "Update veterinarian")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "Veterinarian has updated")]
        [SwaggerResponse(401, Type = typeof(FileContentResult), Description = "Unauthorized!")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "Veterinarian has not updated")]
        public async Task<IActionResult> Update(UpdateVeterinarianCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var result = await _mediator.Send(command, cancellationToken);
                return CreatedAtAction("Update", new { routeValues = result.ResponseObject }, result);
            }
            catch (Exception ex)
            {
                Log.Fatal("VeterinariansController Update exception: ", ex);
                return BadRequest(ex);
            }
        }

        [HttpGet("{id}")]
        [SwaggerOperation(Summary = "Get veterinarian by id", Description = "Returns veterinarian by Id")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "Veterinarian")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "Veterinarian with that id does not exist")]
        public async Task<IActionResult> GetById(Guid id, CancellationToken cancellationToken)
        {
            try
            {
                var query = new GetVeterinarianByIdQuery(id);
                var result = await _mediator.Send(query, cancellationToken);
                return result != null ? (IActionResult)Ok(result) : NotFound();
            }
            catch (Exception ex)
            {
                Log.Fatal("VeterinariansController GetById error", ex);
                return BadRequest(ex);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("Veterinarians")]
        [SwaggerOperation(Summary = "Returns list of veterinarian", Description = "Returns list of veterinarian")]
        [SwaggerResponse(200, Type = typeof(FileContentResult), Description = "List of veterinarian")]
        [SwaggerResponse(401, Type = typeof(FileContentResult), Description = "Unauthorized!")]
        [SwaggerResponse(404, Type = typeof(FileContentResult), Description = "Veterinarian does not exist")]
        public async Task<IActionResult> GetAll(CancellationToken cancellationToken)
        {
            try
            {
                var query = new GetVeterinariansQuery();
                var result = await _mediator.Send(query, cancellationToken);
                return Ok(result);
            }
            catch (Exception ex)
            {
                Log.Fatal("VeterinarianController GetAll exception: ", ex);
                return BadRequest(ex);
            }
        }
    }
}
