﻿using Microsoft.Extensions.Configuration;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using PetClinic.Application.Commands;
using Serilog;
using System;
using System.Threading.Tasks;
using System.Threading;
using Swashbuckle.AspNetCore.Annotations;

namespace PetClinic.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TokenController : Controller
    {
        private readonly IMediator _mediator;
        private readonly IConfiguration _configuration;
        public TokenController(IMediator mediator, IConfiguration configuration)
        {
            _mediator = mediator;
            _configuration = configuration;
        }

        [Route("/token")]
        [HttpPost]
        [SwaggerOperation(Summary = "Create token", Description = "Create token")]
        public async Task<IActionResult> Create(CreateTokenCommand command, CancellationToken cancellationToken)
        {
            try
            {
                command.SetSecurityKey(_configuration["SecurityKey"]);
                var result = await _mediator.Send(command, cancellationToken);
                return CreatedAtAction("Create", new { routeValues = result.ResponseObject }, result);
            }
            catch (Exception ex)
            {
                Log.Fatal("Create token exception: ", ex);
                return BadRequest(ex);
            }
        }
    }
}
