﻿using MediatR;
using PetClinic.Application.Dtos;
using PetClinic.Application.Responses;
using System;

namespace PetClinic.Application.Queries
{
    public class GetVeterinarianByIdQuery : IRequest<DtoObjectResponse>
    {
        public Guid Id { get; }

        public GetVeterinarianByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}
