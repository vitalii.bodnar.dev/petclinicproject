﻿using MediatR;
using PetClinic.Application.Responses;

namespace PetClinic.Application.Queries
{
    public class GetVeterinariansQuery : IRequest<DtoObjectResponse>
    {
    }
}
