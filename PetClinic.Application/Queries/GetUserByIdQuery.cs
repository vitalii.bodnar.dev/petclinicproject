﻿using MediatR;
using PetClinic.Application.Responses;
using System;

namespace PetClinic.Application.Queries
{
    public class GetUserByIdQuery : IRequest<DtoObjectResponse>
    {
        public Guid Id { get; set; }
        public GetUserByIdQuery(Guid id)
        {
            Id = id;
        }
    }
}
