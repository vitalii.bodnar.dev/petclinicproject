﻿using MediatR;
using PetClinic.Application.Filters;
using PetClinic.Application.Responses;

namespace PetClinic.Application.Queries
{
    public class GetVisitsQuery : IRequest<DtoObjectResponse>
    {
        public VisitFilter VisitFilter { get; set; }
        public GetVisitsQuery(VisitFilter visitFilter)
        {
            VisitFilter = visitFilter;
        }
    }
}
