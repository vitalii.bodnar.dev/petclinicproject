﻿using MediatR;
using PetClinic.Application.Responses;

namespace PetClinic.Application.Queries
{
    public class GetVisitByIdQuery : IRequest<DtoObjectResponse>
    {
        public int Id { get; set; }
        public GetVisitByIdQuery(int id)
        {
            Id = id;
        }
    }
}
