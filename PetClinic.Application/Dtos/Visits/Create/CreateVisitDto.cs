﻿using System;

namespace PetClinic.Application.Dtos.Visits.Create
{
    public class CreateVisitDto : VisitBaseDto
    {
        public Guid VeterinarianId { get; set; }
        public Guid CustomerId { get; set; }
        public int AnimalId { get; set; }
    }
}
