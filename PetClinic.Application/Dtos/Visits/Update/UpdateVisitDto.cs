﻿using System;

namespace PetClinic.Application.Dtos.Visits.Update
{
    public class UpdateVisitDto : VisitBaseDto
    {
        public int Id { get; set; }
        public Guid VeterinarianId { get; set; }
        public Guid CustomerId { get; set; }
        public int AnimalId { get; set; }
        public string VeterinarianComment { get; set; }
        public string CustomerComment { get; set; }
    }
}
