﻿using System;

namespace PetClinic.Application.Dtos.Visits
{
    public class VisitBaseDto
    {
        public DateTime VisitTime { get; set; }
        public bool IsVisitingHome { get; set; }
    }
}
