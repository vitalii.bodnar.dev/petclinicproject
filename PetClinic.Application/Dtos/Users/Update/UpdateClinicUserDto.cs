﻿namespace PetClinic.Application.Dtos.Users.Update
{
    public class UpdateClinicUserDto : ClinicUserBaseDto
    {
        public UpdateCustomerInfoDto CustomerInfo { get; set; }
    }
}
