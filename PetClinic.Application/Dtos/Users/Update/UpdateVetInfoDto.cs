﻿namespace PetClinic.Application.Dtos.Users.Update
{
    public class UpdateVetInfoDto : VetInfoBaseDto
    {
        public UpdateVetLicenseDto ActiveLicense { get; set; }
    }
}
