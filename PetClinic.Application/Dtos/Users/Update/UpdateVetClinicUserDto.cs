﻿namespace PetClinic.Application.Dtos.Users.Update
{
    public class UpdateVetClinicUserDto : ClinicUserBaseDto
    {
        public UpdateVetInfoDto VetInfo { get; set; }
    }
}
