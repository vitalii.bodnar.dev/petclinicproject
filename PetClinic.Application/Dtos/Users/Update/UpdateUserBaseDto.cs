﻿using System;

namespace PetClinic.Application.Dtos.Users.Update
{
    public class UpdateUserBaseDto
    {
        public Guid Id { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
    }
}
