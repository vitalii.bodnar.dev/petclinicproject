﻿using PetClinic.Application.Dtos.Animals;
using System.Collections.Generic;

namespace PetClinic.Application.Dtos.Users.Update
{
    public class UpdateCustomerInfoDto : CustomerInfoDtoBase
    {
        public IEnumerable<UpdateAnimalDto> Animals { get; set; }
    }
}
