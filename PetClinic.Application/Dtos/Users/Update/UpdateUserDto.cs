﻿namespace PetClinic.Application.Dtos.Users.Update
{
    public class UpdateUserDto : UpdateUserBaseDto
    {
        public UpdateClinicUserDto ClinicUser { get; set; }
    }
}
