﻿namespace PetClinic.Application.Dtos.Users.Update
{
    public class UpdateVeterinarianDto : UpdateUserBaseDto
    {
        public UpdateVetClinicUserDto ClinicUser { get; set; }
    }
}
