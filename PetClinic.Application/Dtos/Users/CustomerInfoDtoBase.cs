﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetClinic.Application.Dtos.Users
{
    public class CustomerInfoDtoBase
    {
        public int Id { get; set; }
        public Guid ClinicUserId { get; set; }
    }
}
