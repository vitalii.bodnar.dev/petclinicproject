﻿namespace PetClinic.Application.Dtos.Users.Get
{
    public class GetVeterinarianDto : GetApplicationUserDtoBase
    {
        public GetVetClinicUserDto ClinicUser { get; set; }
    }
}
