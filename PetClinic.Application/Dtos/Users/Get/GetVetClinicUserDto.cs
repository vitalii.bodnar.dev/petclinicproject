﻿namespace PetClinic.Application.Dtos.Users.Get
{
    public class GetVetClinicUserDto : ClinicUserBaseDto
    {
        public GetVetInfoDto VetInfo { get; set; }
    }
}
