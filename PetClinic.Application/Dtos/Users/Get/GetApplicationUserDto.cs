﻿namespace PetClinic.Application.Dtos.Users.Get
{
    public class GetApplicationUserDto : GetApplicationUserDtoBase
    {
        public GetClinicUserDto ClinicUser { get; set; }
    }
}
