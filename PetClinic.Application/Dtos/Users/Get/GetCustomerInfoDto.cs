﻿using PetClinic.Application.Dtos.Animals;
using System.Collections.Generic;

namespace PetClinic.Application.Dtos.Users.Get
{
    public class GetCustomerInfoDto : CustomerInfoDtoBase
    {
        public IEnumerable<GetAnimalDto> Animals { get; set; }
    }
}
