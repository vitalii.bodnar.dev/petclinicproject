﻿using System;

namespace PetClinic.Application.Dtos.Users.Get
{
    public class GetClinicUserDto : ClinicUserBaseDto
    {
        public GetCustomerInfoDto CustomerInfo { get; set; }
    }
}
