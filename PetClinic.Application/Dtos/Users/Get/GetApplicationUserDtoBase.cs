﻿using System;
using System.Collections.Generic;

namespace PetClinic.Application.Dtos.Users.Get
{
    public class GetApplicationUserDtoBase
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool EmailConfirmed { get; set; }
        public bool LockoutEnabled { get; set; }
        public IList<ApplicationRoleDto> Roles { get; set; }
    }
}
