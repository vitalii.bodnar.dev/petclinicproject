﻿namespace PetClinic.Application.Dtos.Users.Get
{
    public class GetVetInfoDto : VetInfoBaseDto
    {
        public GetVetLicenseDto ActiveLicense { get; set; }
    }
}
