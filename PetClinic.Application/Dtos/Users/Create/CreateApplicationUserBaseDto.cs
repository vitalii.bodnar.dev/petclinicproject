﻿namespace PetClinic.Application.Dtos.Users.Create
{
    public class CreateApplicationUserBaseDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
