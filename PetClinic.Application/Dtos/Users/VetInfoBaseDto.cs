﻿using System;

namespace PetClinic.Application.Dtos.Users
{
    public class VetInfoBaseDto
    {
        public int Id { get; set; }
        public Guid ClinicUserId { get; set; }
        public string Description { get; set; }
    }
}
