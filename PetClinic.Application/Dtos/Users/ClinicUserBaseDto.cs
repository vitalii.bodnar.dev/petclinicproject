﻿using System;

namespace PetClinic.Application.Dtos.Users
{
    public class ClinicUserBaseDto
    {
        public Guid ApplicationUserId { get; set; }
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
    }
}
