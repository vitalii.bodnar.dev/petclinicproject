﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetClinic.Application.Dtos.Users
{
    public class VetLicenseBaseDto
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public DateTime DateOfIssue { get; set; }
        public DateTime? DateOfExpiry { get; set; }
        public string IssuingAuthorityName { get; set; }
        public string IssuingAuthorityId { get; set; }
    }
}
