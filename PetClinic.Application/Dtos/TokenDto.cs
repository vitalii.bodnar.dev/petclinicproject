﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetClinic.Application.Dtos
{
    public class TokenDto : UserDto
    {
        public string GrantType { get; set; }
    }
}
