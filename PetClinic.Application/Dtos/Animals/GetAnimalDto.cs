﻿namespace PetClinic.Application.Dtos.Animals
{
    public class GetAnimalDto : AnimalBaseDto
    {
        public int OwnerId { get; set; }
    }
}
