﻿using System;

namespace PetClinic.Application.Dtos
{
    public class ApplicationRoleDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
