﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetClinic.Application.Dtos
{
    public class UserDto
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
