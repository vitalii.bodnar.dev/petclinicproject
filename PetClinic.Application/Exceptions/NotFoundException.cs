﻿using System;
using System.Runtime.Serialization;

namespace PetClinic.Application.Exceptions
{
    [Serializable]
    internal class NotFoundException : Exception
    {
        private const string _message = "Entities {0} were not found.";
        private const string _messageWithKey = "Entity {0} with primary key {1} was not found.";

        public NotFoundException(string entityName)
            : base(string.Format(_message, entityName))
        {
        }

        public NotFoundException(string entityName, int key)
            : base(string.Format(_messageWithKey, entityName, key))
        {
        }

        public NotFoundException(string entityName, Guid key)
            : base(string.Format(_messageWithKey, entityName, key))
        {
        }

        protected NotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}