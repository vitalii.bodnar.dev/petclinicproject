﻿using System;

namespace PetClinic.Application.Filters
{
    public class VisitFilter : PagedFilterBase
    {
        public Guid? VeterinarianId { get; set; }
        public Guid? CustomerId { get; set; }
        public int? AnimalId { get; set; }
        public int? AnimalType { get; set; }
        public bool? IsVisitingHome { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }
}
