﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PetClinic.Application.Responses
{
    public enum OperationResultCode
    {
        Ok = 1,
        Error = 2
    }
}
