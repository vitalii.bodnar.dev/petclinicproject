﻿using System;
using System.Collections.Generic;

namespace PetClinic.Application.Responses
{
    [Serializable]
    public class DtoObjectResponse
    {
        public OperationResultCode Code { get; set; }
        public object ResponseObject { get; set; }
        public IEnumerable<string> ErrorMessages { get; set; }

        public static DtoObjectResponse Ok(object responseObject)
        {
            return new DtoObjectResponse
            {
                Code = OperationResultCode.Ok,
                ResponseObject = responseObject,
                ErrorMessages = new List<string>()
            };
        }

        public static DtoObjectResponse Error(IEnumerable<string> errorMessages)
        {
            return new DtoObjectResponse
            {
                Code = OperationResultCode.Error,
                ErrorMessages = errorMessages
            };
        }
    }
}
