﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using PetClinic.Application.Dtos.Users.Get;
using PetClinic.Application.Exceptions;
using PetClinic.Application.Queries;
using PetClinic.Application.Responses;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class GetUserByIdHandler : IRequestHandler<GetUserByIdQuery, DtoObjectResponse>
    {
        private readonly IApplicationUserService _service;
        private readonly ILogger<GetUserByIdHandler> _logger;
        private readonly IMapper _mapper;

        public GetUserByIdHandler(IApplicationUserService service, ILogger<GetUserByIdHandler> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<DtoObjectResponse> Handle(GetUserByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var applicationUser = await _service.GetAsync(request.Id);
                if (applicationUser == null)
                {
                    throw new NotFoundException(nameof(GetApplicationUserDto), request.Id);
                }
                var objectResponse = _mapper.Map<GetApplicationUserDto>(applicationUser);
                return DtoObjectResponse.Ok(objectResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("GetUserByIdHandler Handle exception: ", ex);
                return DtoObjectResponse.Error(new List<string>() { ex.Message });
            }
        }
    }
}
