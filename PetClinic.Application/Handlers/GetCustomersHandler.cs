﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using PetClinic.Application.Dtos.Users.Get;
using PetClinic.Application.Exceptions;
using PetClinic.Application.Queries;
using PetClinic.Application.Responses;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class GetCustomersHandler : IRequestHandler<GetCustomersQuery, DtoObjectResponse>
    {
        private readonly IApplicationUserService _service;
        private readonly ILogger<GetCustomersHandler> _logger;
        private readonly IMapper _mapper;

        public GetCustomersHandler(IApplicationUserService service, ILogger<GetCustomersHandler> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<DtoObjectResponse> Handle(GetCustomersQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var customers = await _service.GetCustomers();
                if (customers == null || !customers.Any())
                {
                    throw new NotFoundException(nameof(GetApplicationUserDto));
                }
                
                var objectResponse = _mapper.Map<List<GetApplicationUserDto>>(customers);
                return DtoObjectResponse.Ok(objectResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("GetCustomersHandler Handle exception: ", ex);
                return DtoObjectResponse.Error(new List<string>() { ex.Message });
            }
        }
    }
}
