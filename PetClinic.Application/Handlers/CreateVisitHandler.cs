﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using PetClinic.Application.Commands;
using PetClinic.Application.Dtos.Visits.Get;
using PetClinic.Application.Responses;
using PetClinic.Domain.Models;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class CreateVisitHandler : IRequestHandler<CreateVisitCommand, DtoObjectResponse>
    {
        private readonly IVisitService _service;
        private readonly ILogger<CreateVisitHandler> _logger;
        private readonly IMapper _mapper;

        public CreateVisitHandler(IVisitService service, ILogger<CreateVisitHandler> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<DtoObjectResponse> Handle(CreateVisitCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var visit = await _service.CreateVisit(_mapper.Map<CreateVisitModel>(request));
                var objectResponse = _mapper.Map<GetVisitDto>(visit);
                return DtoObjectResponse.Ok(objectResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("CreateVisitHandler Handle exception: ", ex);
                return DtoObjectResponse.Error(new List<string>() { ex.Message });
            }
        }
    }
}
