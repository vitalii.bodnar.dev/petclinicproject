﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using PetClinic.Application.Commands;
using PetClinic.Application.Dtos.Users.Get;
using PetClinic.Application.Responses;
using PetClinic.Domain.Models;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class CreateVeterinarianHandler : IRequestHandler<CreateVeterinarianCommand, DtoObjectResponse>
    {
        private readonly ILogger<CreateVeterinarianHandler> _logger;
        private readonly IApplicationUserService _service;
        private readonly IMapper _mapper;

        public CreateVeterinarianHandler(IApplicationUserService service, ILogger<CreateVeterinarianHandler> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<DtoObjectResponse> Handle(CreateVeterinarianCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var createUserModel = _mapper.Map<CreateUserModel>(request);
                await _service.CreateVeterinarian(createUserModel);
                var applicationUser = await _service.FindByEmailAsync(request.Email);
                var objectResponse = _mapper.Map<GetApplicationUserDto>(applicationUser);
                return DtoObjectResponse.Ok(objectResponse);
            }
            catch(Exception ex)
            {
                _logger.LogError("CreateVeterinarianHandler Handle exception: ", ex);
                return DtoObjectResponse.Error(new List<string>() { ex.Message });
            }
        }
    }
}
