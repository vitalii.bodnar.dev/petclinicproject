﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using PetClinic.Application.Dtos;
using PetClinic.Application.Dtos.Visits.Get;
using PetClinic.Application.Exceptions;
using PetClinic.Application.Queries;
using PetClinic.Application.Responses;
using PetClinic.Domain.Models;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class GetVisitsHandler : IRequestHandler<GetVisitsQuery, DtoObjectResponse>
    {
        private readonly IVisitService _service;
        private readonly ILogger<GetVisitsHandler> _logger;
        private readonly IMapper _mapper;

        public GetVisitsHandler(IVisitService service, ILogger<GetVisitsHandler> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }
        public async Task<DtoObjectResponse> Handle(GetVisitsQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var pagedResult = await _service.GetVisitsByFilter(_mapper.Map<VisitFilterModel>(request.VisitFilter));
                if (pagedResult == null || !pagedResult.Results.Any())
                {
                    throw new NotFoundException(nameof(GetVisitDto));
                }
                var objectResponse = _mapper.Map<PagedResultDto<GetVisitDto>>(pagedResult);
                return DtoObjectResponse.Ok(objectResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("GetVisitsHandler Handle exception: ", ex);
                return DtoObjectResponse.Error(new List<string>() { ex.Message });
            }
        }
    }
}
