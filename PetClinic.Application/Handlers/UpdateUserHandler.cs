﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using PetClinic.Application.Commands;
using PetClinic.Application.Dtos.Users.Update;
using PetClinic.Application.Responses;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class UpdateUserHandler : IRequestHandler<UpdateUserCommand, DtoObjectResponse>
    {
        private readonly IApplicationUserService _service;
        private readonly ILogger<UpdateUserHandler> _logger;
        private readonly IMapper _mapper;

        public UpdateUserHandler(IApplicationUserService service, ILogger<UpdateUserHandler> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<DtoObjectResponse> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var user = await _service.GetAsync(request.Id, cancellationToken);
                if (user == null)
                {
                    throw new NullReferenceException($"User with id: {request.Id} does not exist!");
                }
                var modifiedUser = _mapper.Map(request, user);
                await _service.UpdateAsync(modifiedUser, cancellationToken);
                var objectResponse = _mapper.Map<UpdateUserDto>(await _service.GetAsync(request.Id));
                return DtoObjectResponse.Ok(objectResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("UpdateUserHandler Handle exception: ", ex);
                return DtoObjectResponse.Error(new List<string>() { ex.Message });
            }
        }
    }
}
