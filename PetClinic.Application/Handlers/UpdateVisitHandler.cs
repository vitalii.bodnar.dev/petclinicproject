﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using PetClinic.Application.Commands;
using PetClinic.Application.Dtos.Visits.Get;
using PetClinic.Application.Responses;
using PetClinic.Domain.Models;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class UpdateVisitHandler : IRequestHandler<UpdateVisitCommand, DtoObjectResponse>
    {
        private readonly IVisitService _service;
        private readonly ILogger<UpdateVisitHandler> _logger;
        private readonly IMapper _mapper;

        public UpdateVisitHandler(IVisitService service, ILogger<UpdateVisitHandler> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<DtoObjectResponse> Handle(UpdateVisitCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var visit = await _service.UpdateAsync(_mapper.Map<UpdateVisitModel>(request), cancellationToken);
                var objectResponse = _mapper.Map<GetVisitDto>(visit);
                return DtoObjectResponse.Ok(objectResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("UpdateVisitHandler Handle exception: ", ex);
                return DtoObjectResponse.Error(new List<string>() { ex.Message });
            }
        }
    }
}
