﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using PetClinic.Application.Dtos.Users.Get;
using PetClinic.Application.Exceptions;
using PetClinic.Application.Queries;
using PetClinic.Application.Responses;
using PetClinic.Service.Services;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class GetVeterinarianByIdHandler : IRequestHandler<GetVeterinarianByIdQuery, DtoObjectResponse>
    {
        private readonly ILogger<GetVeterinarianByIdHandler> _logger;
        private readonly IApplicationUserService _service;
        private readonly IMapper _mapper;

        public GetVeterinarianByIdHandler(IApplicationUserService service, ILogger<GetVeterinarianByIdHandler> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<DtoObjectResponse> Handle(GetVeterinarianByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var applicationUser = await _service.GetAsync(request.Id);
                if (applicationUser == null)
                {
                    throw new NotFoundException(nameof(GetVeterinarianDto), request.Id);
                }
                var objectResponse = _mapper.Map<GetVeterinarianDto>(applicationUser);
                return DtoObjectResponse.Ok(objectResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("GetVeterinarianByIdHandler Handle exception: ", ex);
                return null;
            }
        }
    }
}
