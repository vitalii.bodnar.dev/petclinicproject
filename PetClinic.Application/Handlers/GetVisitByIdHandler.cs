﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using PetClinic.Application.Dtos.Visits.Get;
using PetClinic.Application.Queries;
using PetClinic.Application.Responses;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class GetVisitByIdHandler : IRequestHandler<GetVisitByIdQuery, DtoObjectResponse>
    {
        private readonly IVisitService _service;
        private readonly ILogger<GetVisitByIdHandler> _logger;
        private readonly IMapper _mapper;

        public GetVisitByIdHandler(IVisitService service, ILogger<GetVisitByIdHandler> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<DtoObjectResponse> Handle(GetVisitByIdQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var visit = await _service.GetAsync(request.Id, cancellationToken);
                var objectResponse = _mapper.Map<GetVisitDto>(visit);
                return DtoObjectResponse.Ok(objectResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("GetVisitByIdHandler Handle exception: ", ex);
                return DtoObjectResponse.Error(new List<string>() { ex.Message });
            }
        }
    }
}
