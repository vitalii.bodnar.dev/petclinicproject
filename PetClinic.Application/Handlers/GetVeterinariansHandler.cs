﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using PetClinic.Application.Dtos.Users.Get;
using PetClinic.Application.Exceptions;
using PetClinic.Application.Queries;
using PetClinic.Application.Responses;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class GetVeterinariansHandler : IRequestHandler<GetVeterinariansQuery, DtoObjectResponse>
    {
        private readonly IApplicationUserService _service;
        private readonly ILogger<GetVeterinariansHandler> _logger;
        private readonly IMapper _mapper;

        public GetVeterinariansHandler(IApplicationUserService service, ILogger<GetVeterinariansHandler> logger, IMapper mapper)
        {
            _service = service;
            _logger = logger;
            _mapper = mapper;
        }

        public async Task<DtoObjectResponse> Handle(GetVeterinariansQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var veterinarians = await _service.GetVeterinarians();
                if (veterinarians == null || !veterinarians.Any())
                {
                    throw new NotFoundException(nameof(GetVeterinarianDto));
                }
                var objectResponse = _mapper.Map<List<GetVeterinarianDto>>(veterinarians);
                return DtoObjectResponse.Ok(objectResponse);
            }
            catch (Exception ex)
            {
                _logger.LogError("GetVeterinariansHandler Handle exception: ", ex);
                return DtoObjectResponse.Error(new List<string>() { ex.Message });
            }
        }
    }
}
