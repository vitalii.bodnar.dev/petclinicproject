﻿using MediatR;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using PetClinic.Application.Commands;
using PetClinic.Application.Responses;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PetClinic.Application.Handlers
{
    public class CreateTokenHandler : IRequestHandler<CreateTokenCommand, DtoObjectResponse>
    {
        private readonly ILogger<CreateTokenHandler> _logger;
        private readonly IApplicationUserService _service;
        public CreateTokenHandler(IApplicationUserService service, ILogger<CreateTokenHandler> logger)
        {
            _service = service;
            _logger = logger;
        }

        public async Task<DtoObjectResponse> Handle(CreateTokenCommand request, CancellationToken cancellationToken)
        {
            try
            {
                if(!await IsValidUserNameAndPassword(request.Email, request.Password, cancellationToken) || string.IsNullOrEmpty(request.GetSecurityKey()))
                {
                    return DtoObjectResponse.Error(new List<string>() { "User is not valid!" });
                }

                if (string.IsNullOrEmpty(request.GetSecurityKey()))
                {
                    return DtoObjectResponse.Error(new List<string>() { "SecurityKey not valid!" });
                }

                var token = await GenerateToken(request.Email, request.GetSecurityKey(), cancellationToken);
                return DtoObjectResponse.Ok(token);
            }
            catch (Exception ex)
            {
                _logger.LogError("CreateTokenHandler Handle exception: ", ex);
                return DtoObjectResponse.Error(new List<string>() { ex.Message });
            }
        }

        private async Task<bool> IsValidUserNameAndPassword(string userName, string password, CancellationToken cancellationToken)
        {
            var user = await _service.FindByEmailAsync(userName, cancellationToken);
            return _service.CheckPassword(user, password);
        }

        private async Task<dynamic> GenerateToken(string userName, string securityKey, CancellationToken cancellationToken)
        {
            try
            {
                var user = await _service.FindByEmailAsync(userName, cancellationToken);
                var roles = user.Roles;

                var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, userName),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(JwtRegisteredClaimNames.Nbf, new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Exp, new DateTimeOffset(DateTime.Now.AddHours(1)).ToUnixTimeSeconds().ToString())
            };

                foreach (var role in roles)
                {
                    claims.Add(new Claim(ClaimTypes.Role, role.Name));
                }

                var token = new JwtSecurityToken(
                        new JwtHeader(
                            new SigningCredentials(
                                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(securityKey)), SecurityAlgorithms.HmacSha256)),
                        new JwtPayload(claims));

                var output = new
                {
                    Access_Token = new JwtSecurityTokenHandler().WriteToken(token),
                    UserName = userName,
                    UserId = user.Id
                };

                return output;
            }
            catch (Exception ex)
            {
                _logger.LogError("CreateTokenHandler GenerateToken exception: ", ex);
                return null;
            }
        }
    }
}
