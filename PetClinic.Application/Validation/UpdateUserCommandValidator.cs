﻿using FluentValidation;
using PetClinic.Application.Commands;
using PetClinic.Service.Services;

namespace PetClinic.Application.Validation
{
    public class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserCommandValidator(IApplicationUserService service)
        {
            RuleFor(user => user.ClinicUser.FirstName)
                .NotEmpty()
                .WithMessage("FirstName is required.");
            RuleFor(user => user.ClinicUser.LastName)
                .NotEmpty()
                .WithMessage("LastName is required.");
            RuleFor(user => user.PhoneNumber)
                .NotEmpty()
                .WithMessage("PhoneNumber is required.");
            RuleFor(user => user.Email)
                .NotEmpty()
                .WithMessage("Email is required.")
                .EmailAddress()
                .WithMessage("Invalid email format.")
                .Must((user, email) => service.IsEmailUnique(email, user.Id))
                .WithMessage("Email already taken");
        }
    }
}
