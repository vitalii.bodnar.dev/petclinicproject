﻿using FluentValidation;
using PetClinic.Application.Commands;

namespace PetClinic.Application.Validation
{
    public class UpdateVisitCommandValidator : AbstractValidator<UpdateVisitCommand>
    {
        public UpdateVisitCommandValidator()
        {
            RuleFor(user => user.AnimalId)
                .NotEmpty()
                .WithMessage("Animal is required.");
            RuleFor(user => user.CustomerId)
                .NotEmpty()
                .WithMessage("Customer is required.");
            RuleFor(user => user.VeterinarianId)
                .NotEmpty()
                .WithMessage("Veterinarian is required.");
            RuleFor(user => user.VisitTime)
                .NotEmpty()
                .WithMessage("VisitTime is required.");
        }
    }
}
