﻿using FluentValidation;
using PetClinic.Application.Commands;
using PetClinic.Service.Services;
using System;
using System.Collections.Generic;
using System.Text;

namespace PetClinic.Application.Validation
{
    public class CreateTokenCommandValidator : AbstractValidator<CreateTokenCommand>
    {
        public CreateTokenCommandValidator(IApplicationUserService service)
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email is required.")
                .EmailAddress()
                .WithMessage("Invalid email format.");
            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password is required.")
                .MinimumLength(8)
                .WithMessage("Password must be longer than 8 characters.")
                .Must(service.IsPasswordValid)
                .WithMessage("Sorry password didn't satisfy the custom logic");
            RuleFor(x => x.GrantType).NotEmpty();
        }
    }
}
