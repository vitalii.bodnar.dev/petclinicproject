﻿using FluentValidation;
using PetClinic.Application.Commands;
using PetClinic.Service.Services;

namespace PetClinic.Application.Validation
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator(IApplicationUserService service)
        {
            RuleFor(x => x.Email)
                .NotEmpty()
                .WithMessage("Email is required.")
                .EmailAddress()
                .WithMessage("Invalid email format.")
                .Must(x => service.IsEmailUnique(x))
                .WithMessage("Email already taken");
            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("Password is required.")
                .MinimumLength(8)
                .WithMessage("Password must be longer than 8 characters.")
                .Must(service.IsPasswordValid)
                .WithMessage("Sorry password didn't satisfy the custom logic");
            RuleFor(x => x.FirstName)
                .NotEmpty()
                .WithMessage("First name is required.");
            RuleFor(x => x.LastName)
                .NotEmpty()
                .WithMessage("Last name is required.");
        }
    }
}
