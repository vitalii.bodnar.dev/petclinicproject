﻿using MediatR;
using PetClinic.Application.Dtos.Visits.Update;
using PetClinic.Application.Responses;

namespace PetClinic.Application.Commands
{
    public class UpdateVisitCommand : UpdateVisitDto, IRequest<DtoObjectResponse>
    {
    }
}
