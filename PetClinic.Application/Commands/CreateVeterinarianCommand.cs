﻿using MediatR;
using PetClinic.Application.Dtos.Users.Create;
using PetClinic.Application.Responses;

namespace PetClinic.Application.Commands
{
    public class CreateVeterinarianCommand : CreateVeterinarianDto, IRequest<DtoObjectResponse>
    {
    }
}
