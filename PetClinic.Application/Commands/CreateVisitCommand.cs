﻿using MediatR;
using PetClinic.Application.Dtos.Visits.Create;
using PetClinic.Application.Responses;

namespace PetClinic.Application.Commands
{
    public class CreateVisitCommand : CreateVisitDto, IRequest<DtoObjectResponse>
    {
    }
}
