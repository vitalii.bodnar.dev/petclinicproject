﻿using MediatR;
using PetClinic.Application.Dtos.Users.Update;
using PetClinic.Application.Responses;

namespace PetClinic.Application.Commands
{
    public class UpdateUserCommand : UpdateUserDto, IRequest<DtoObjectResponse>
    {
    }
}
