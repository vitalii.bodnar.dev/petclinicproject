﻿using MediatR;
using PetClinic.Application.Dtos;
using PetClinic.Application.Responses;

namespace PetClinic.Application.Commands
{
    public class CreateTokenCommand : TokenDto, IRequest<DtoObjectResponse>
    {
        private string _securityKey;
        public void SetSecurityKey(string securityKey)
        {
            _securityKey = securityKey;
        }

        public string GetSecurityKey()
        {
            return _securityKey;
        }
    }
}
