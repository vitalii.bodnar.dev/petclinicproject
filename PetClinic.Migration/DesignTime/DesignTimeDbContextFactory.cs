﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using PetClinic.DbMigration.DataAccess;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PetClinic.DbMigration.DesignTime
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ApplicationDbContext>
    {
        public ApplicationDbContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ApplicationDbContext>();
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var connectionString = configuration["ConnectionString:Default"];

            if (string.IsNullOrEmpty(connectionString))
            {
                throw new NullReferenceException("DbMigration DesignTimeDbContextFactory ConnectionString Is null or empty!");
            }

            optionsBuilder.UseSqlServer(connectionString);
            return new ApplicationDbContext(optionsBuilder.Options);
        }
    }
}
