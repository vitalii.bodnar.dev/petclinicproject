﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PetClinic.DbMigration.Migrations
{
    public partial class DeleteOldVet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VetInformations_Licenses_ActiveLicenseId",
                table: "VetInformations");

            migrationBuilder.DropTable(
                name: "Veterinarians");

            migrationBuilder.DropIndex(
                name: "IX_VetInformations_ActiveLicenseId",
                table: "VetInformations");

            migrationBuilder.DropColumn(
                name: "ActiveLicenseId",
                table: "VetInformations");

            migrationBuilder.AddColumn<int>(
                name: "VetInfoId",
                table: "Licenses",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Licenses_VetInfoId",
                table: "Licenses",
                column: "VetInfoId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Licenses_VetInformations_VetInfoId",
                table: "Licenses",
                column: "VetInfoId",
                principalTable: "VetInformations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Licenses_VetInformations_VetInfoId",
                table: "Licenses");

            migrationBuilder.DropIndex(
                name: "IX_Licenses_VetInfoId",
                table: "Licenses");

            migrationBuilder.DropColumn(
                name: "VetInfoId",
                table: "Licenses");

            migrationBuilder.AddColumn<int>(
                name: "ActiveLicenseId",
                table: "VetInformations",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Veterinarians",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ActiveLicenseId = table.Column<int>(type: "int", nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "date", nullable: false),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    DocumentNumber = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    EmailAddress = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: false),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PhoneNumber = table.Column<string>(type: "nvarchar(12)", maxLength: 12, nullable: false),
                    SecondName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Veterinarians", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Veterinarians_Licenses_ActiveLicenseId",
                        column: x => x.ActiveLicenseId,
                        principalTable: "Licenses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VetInformations_ActiveLicenseId",
                table: "VetInformations",
                column: "ActiveLicenseId");

            migrationBuilder.CreateIndex(
                name: "IX_Veterinarians_ActiveLicenseId",
                table: "Veterinarians",
                column: "ActiveLicenseId");

            migrationBuilder.AddForeignKey(
                name: "FK_VetInformations_Licenses_ActiveLicenseId",
                table: "VetInformations",
                column: "ActiveLicenseId",
                principalTable: "Licenses",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
