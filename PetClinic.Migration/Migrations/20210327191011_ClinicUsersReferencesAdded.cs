﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PetClinic.DbMigration.Migrations
{
    public partial class ClinicUsersReferencesAdded : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ApplicationUsers_ClinicUsers_ClinicUserId",
                table: "ApplicationUsers");

            migrationBuilder.DropIndex(
                name: "IX_ApplicationUsers_ClinicUserId",
                table: "ApplicationUsers");

            migrationBuilder.DropColumn(
                name: "ClinicUserId",
                table: "ApplicationUsers");

            migrationBuilder.AddColumn<Guid>(
                name: "ApplicationUserId",
                table: "ClinicUsers",
                type: "uniqueidentifier",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.CreateIndex(
                name: "IX_ClinicUsers_ApplicationUserId",
                table: "ClinicUsers",
                column: "ApplicationUserId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ClinicUsers_ApplicationUsers_ApplicationUserId",
                table: "ClinicUsers",
                column: "ApplicationUserId",
                principalTable: "ApplicationUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ClinicUsers_ApplicationUsers_ApplicationUserId",
                table: "ClinicUsers");

            migrationBuilder.DropIndex(
                name: "IX_ClinicUsers_ApplicationUserId",
                table: "ClinicUsers");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "ClinicUsers");

            migrationBuilder.AddColumn<Guid>(
                name: "ClinicUserId",
                table: "ApplicationUsers",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ApplicationUsers_ClinicUserId",
                table: "ApplicationUsers",
                column: "ClinicUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ApplicationUsers_ClinicUsers_ClinicUserId",
                table: "ApplicationUsers",
                column: "ClinicUserId",
                principalTable: "ClinicUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
