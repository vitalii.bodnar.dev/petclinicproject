﻿using Microsoft.EntityFrameworkCore;
using PetClinic.Domain.AggregateModels;
using PetClinic.Domain.AggregateModels.ClinicUserDetails;
using PetClinic.Domain.ApplicationUserModels;

namespace PetClinic.DbMigration.DataAccess
{
    //add-migration name -verbose
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options) {  }

        public DbSet<VetLicense> Licenses { get; set; }
        public DbSet<ApplicationRole> ApplicationRoles { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<CustomerInfo> CustomerInformations { get; set; }
        public DbSet<VetInfo> VetInformations { get; set; }
        public DbSet<ClinicUser> ClinicUsers { get; set; }
        public DbSet<Visit> Visits { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<ApplicationRole>(entity => {
                entity.Property(role => role.Id).IsRequired();
                entity.Property(role => role.ConcurrencyStamp).HasMaxLength(250);
                entity.Property(role => role.Name).HasMaxLength(100).IsRequired();
                entity.Property(role => role.NormalizedName).HasMaxLength(100);
                entity.Property(role => role.Description).HasMaxLength(250).IsRequired();
            });

            builder.Entity<VetInfo>(entity =>
            {
                entity.HasOne(x => x.ActiveLicense)
                    .WithOne(x => x.VetInfo)
                    .HasForeignKey<VetLicense>(x => x.VetInfoId);
            });

            builder.Entity<CustomerInfo>(entity => 
            {
                entity.HasMany(x => x.Animals).WithOne(x => x.Owner);
            });

            builder.Entity<ApplicationUser>(entity =>
            {
                entity.HasMany(x => x.Roles)
                    .WithMany(x => x.Users);
                entity.HasOne(x => x.ClinicUser)
                    .WithOne(x => x.ApplicationUser)
                    .HasForeignKey<ClinicUser>(x => x.ApplicationUserId);
            });

            builder.Entity<ClinicUser>(entity =>
            {
                entity.HasOne(a => a.CustomerInfo)
                    .WithOne(b => b.ClinicUser)
                    .HasForeignKey<CustomerInfo>(x => x.ClinicUserId);
                entity.HasOne(x => x.VetInfo)
                    .WithOne(x => x.ClinicUser)
                    .HasForeignKey<VetInfo>(x => x.ClinicUserId);
            });

            builder.Entity<ApplicationUser>(entity =>
            {
                entity.Property(user => user.Id).IsRequired();
                entity.Property(user => user.UserName).HasMaxLength(100).IsRequired();
                entity.Property(user => user.PasswordHash).HasMaxLength(250).IsRequired();
                entity.Property(user => user.PasswordSalt).HasMaxLength(250).IsRequired();
                entity.Property(user => user.SecurityStamp).HasMaxLength(250);
                entity.Property(user => user.ConcurrencyStamp).HasMaxLength(250);
                entity.Property(user => user.PhoneNumber).HasMaxLength(12).IsRequired();
                entity.Property(user => user.Email).HasMaxLength(100).IsRequired();
                entity.Property(user => user.NormalizedEmail).HasMaxLength(100);
                entity.Property(user => user.NormalizedUserName).HasMaxLength(100);
                entity.Property(user => user.TwoFactorEnabled).IsRequired().HasDefaultValue(false);
                entity.Property(user => user.EmailConfirmed).IsRequired().HasDefaultValue(false);
            });

            builder.Entity<Visit>(entity =>
            {
                entity.HasOne(x => x.Animal)
                    .WithMany()
                    .HasForeignKey(x => x.AnimalId)
                    .OnDelete(DeleteBehavior.NoAction);
                entity.HasOne(x => x.Customer)
                    .WithMany()
                    .HasForeignKey(x => x.CustomerId)
                    .OnDelete(DeleteBehavior.NoAction);
                entity.HasOne(x => x.Veterinarian)
                    .WithMany()
                    .HasForeignKey(x => x.VeterinarianId)
                    .OnDelete(DeleteBehavior.NoAction);
            });

            base.OnModelCreating(builder);
        }
    }
}
